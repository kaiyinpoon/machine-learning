import csv

class PlayerSeason:
    def __init__(self, uniqueId, name, season, ws, year, pick, lastWS, last2WS, last3WS):
        self.uniqueId, self.name, self.season, self.ws = uniqueId, name, season, ws
        self.year, self.pick, self.lastWS = year, pick, lastWS
        self.last2WS, self.last3WS = last2WS, last3WS

playerSeasons = []
player = ""
lastWS, last2WS, last3WS = 0, 0, 0
with open("nba-draft-player-ws.csv") as wsFile:
    wsReader = csv.reader(wsFile)
    for row in wsReader:
        if row[0] == "Id":
            continue
        if row[1] != player:
            player = row[1]
            lastWS, last2WS, last3WS = 0, 0, 0
        playerSeasons.append(PlayerSeason(row[0], row[1], row[2], row[3], row[4], row[5], lastWS, last2WS, last3WS))
        lastWS, last2WS, last3WS = round(float(row[3]), 1), lastWS, last2WS

def getCsvRow(playerSeason):
    return [playerSeason.uniqueId, playerSeason.name, playerSeason.season, playerSeason.ws, playerSeason.year, playerSeason.pick, playerSeason.lastWS, playerSeason.last2WS, playerSeason.last3WS]

with open("nba-draft-player-ws-full.csv", "w", newline="") as draftFile:
    draftWriter = csv.writer(draftFile, quoting=csv.QUOTE_ALL)
    draftWriter.writerow(["Id", "Name", "Season", "WS", "Year", "Pick", "Last WS", "Last 2 WS", "Last 3 WS"])
    draftWriter.writerows(list(map(getCsvRow, playerSeasons)))
