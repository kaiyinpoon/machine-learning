1.
	nba-odds-yyyy-yy.csv
		NBA odds data from http://www.sportsbookreviewsonline.com/scoresoddsarchives/nba/nbaoddsarchives.htm
	team-map.csv and team-code.csv
		Mapping between team names across different websites
	nba-elo-2007-08.csv
		Elo rating of teams at the start of 2007-08 season, from https://projects.fivethirtyeight.com/complete-history-of-the-nba/
	distance.csv
		Great circle distance between airport of NBA cities, from http://www.gcmap.com/
2.
	nbaGameBasicDataMining.py
		Extract info and create variables from nba-odds-yyyy-yy.csv; produces nba-games-yyyy-yy.csv
3.
	nbaDraftDataMining.py
		Get players drafted in 1973 and beyond; produces nba-draft-1973-06.csv, from Basketball-Reference
4.
	nbaProcessDraftPlayers.py and nbaPostProcessDraftPlayers.py
		Get win shares data from drafted players in nba-draft-1973-06.csv; produces nba-draft-ws.csv and nba-draft-players-ws-full.csv
5.
	nba-ws-projection-model.R
		Train polynomial model to project win shares from draft position, past win shares, and experience
6.
	nbaStartersDataMining.py
		Extract URLs from nba-games-yyyy-yy.csv and obtain starters list for each game; produces nba-starters-2007-17.csv (set of all players who started in that span) and nba-game-starters-2007-17.csv (list of starters grouped by individual games)
7.
	nbaStartersWSDataMining.py
		Get win shares data from all starters in nba-starters-2007-17.csv; produces nba-starter-ws-full.csv
8.
	nbaProcessEverything.py
		Merge game data from nba-games-yyyy-yy.csv (list of games with Elo, odds, created variables), nba-game-starters-2007-17.csv (list of games with starters), nba-starter-ws-full.csv (list of player seasons with win shares data); produces nba-games-2007-17-full.csv
9. # Train Forecast