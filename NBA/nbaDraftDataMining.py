from html.parser import HTMLParser
import urllib.request
import csv

class DraftedPlayer:
    def __init__(self, draft, pick, name, link):
        self.draft, self.pick, self.name, self.link = draft, pick, name, link

class DraftParser(HTMLParser):
    def __init__(self, draft):
        HTMLParser.__init__(self)
        self.draft = draft
        self.pick = 0
        self.playerTag = False
        self.player = ""
        self.yearsTag = False
        self.years = ""
        self.link = ""
        self.players = []

    def handle_starttag(self, tag, attributes):
        if tag == "td":
            for name, value in attributes:
                if name == "data-stat" and value == "pick_overall":
                    self.pick += 1
                if name == "data-stat" and value == "seasons":
                    self.yearsTag = True
        else:
            self.yearsTag = False
        if tag == "a":
            for name, value in attributes:
                if name == "href" and "players" in value:
                    self.link = value
                    self.playerTag = True
        else:
            self.playerTag = False

    def handle_data(self, data):
        if self.yearsTag:
            self.yearsTag = False
            try:
                self.years = int(data)
                self.players.append(DraftedPlayer(self.draft, self.pick, self.player, self.link))
            except ValueError:
                pass
        if self.playerTag:
            self.playerTag = False
            self.player = data

players = []
""" 1973-74 season is the first with reliable win shares data"""
for year in range(1973, 2007):
    with urllib.request.urlopen("https://www.basketball-reference.com/draft/NBA_" + str(year) + ".html") as url:
        p = DraftParser(year)
        html = str(url.read())
        p.feed(html)
        players.extend(p.players)
        p.close()

def getCsvRow(player):
    return [player.pick, player.name, player.link[11: -5], player.link]

with open("nba-draft-1973-06.csv", "w", newline="") as draftFile:
    draftWriter = csv.writer(draftFile, quoting=csv.QUOTE_ALL)
    draftWriter.writerow(["Pick", "Name", "Id", "Link"])
    draftWriter.writerows(list(map(getCsvRow, players)))
