import csv

class PlayerSeason:
    def __init__(self, uniqueId, year, season, winShares, pick, lastWS, last2WS, last3WS, totalLast3WS, projectedWS, projectedWSAdj):
        self.uniqueId, self.year = uniqueId, year
        self.season, self.winShares, self.pick = season, winShares, pick
        self.lastWS, self.last2WS, self.last3WS = lastWS, last2WS, last3WS
        self.totalLast3WS, self.projectedWS, self.projectedWSAdj = totalLast3WS, projectedWS, projectedWSAdj

playerSeasons = []
with open("nba-starter-ws-full.csv") as file:
    reader = csv.reader(file)
    for row in reader:
        if row[0] == "Id":
            continue
        playerSeasons.append(PlayerSeason(row[0], row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[8], row[9], row[10]))

playerSeasonMap = dict()
player = ""
for playerSeason in playerSeasons:
    if playerSeason.uniqueId != player:
        player = playerSeason.uniqueId
        playerSeasonMap[player] = dict()
    playerSeasonMap[player][playerSeason.year] = playerSeason

years = range(2007, 2017)
gameStarters = dict()
gameInfo = []

class GameStarters:
    def __init__(self, gameId, homeStarter1, homeStarter2, homeStarter3, homeStarter4, homeStarter5, awayStarter1, awayStarter2, awayStarter3, awayStarter4, awayStarter5):
        self.gameId, self.homeStarter1, self.homeStarter2 = gameId, homeStarter1, homeStarter2
        self.homeStarter3, self.homeStarter4, self.homeStarter5 = homeStarter3, homeStarter4, homeStarter5
        self.awayStarter1, self.awayStarter2, self.awayStarter3 = awayStarter1, awayStarter2, awayStarter3
        self.awayStarter4, self.awayStarter5 = awayStarter4, awayStarter5

class GameInfo:
    def __init__(self, row):
        self.gameId, self.date, self.season, self.homeTeam = row[0], row[1], row[2], row[3]
        self.homeScore, self.homeEloPre, self.homeEloPost, self.homeDaysRest = row[4], row[5], row[6], row[7]
        self.homeFourthInFiveDays, self.homeFifthInSevenDays, self.homeDistance, self.homeOdds = row[8], row[9], row[10], row[11]
        self.homeProb, self.homeLogit, self.awayTeam, self.awayScore = row[12], row[13], row[14], row[15]
        self.awayEloPre, self.awayEloPost, self.awayDaysRest, self.awayFourthInFiveDays = row[16], row[17], row[18], row[19]
        self.awayFifthInSevenDays, self.awayDistance, self.awayOdds, self.eloDiff = row[20], row[21], row[22], row[23]
        self.isHomeVictory = row[24]
        self.homeStarter1, self.homeStarter2, self.homeStarter3, self.homeStarter4, self.homeStarter5 = "", "", "", "", ""
        self.homeStarter1WS, self.homeStarter1ProjectedWS, self.homeStarter1LastWS, self.homeStarter1Last2WS, self.homeStarter1Last3WS, self.homeStarter1TotalLast3WS, self.homeStarter1ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.homeStarter2WS, self.homeStarter2ProjectedWS, self.homeStarter2LastWS, self.homeStarter2Last2WS, self.homeStarter2Last3WS, self.homeStarter2TotalLast3WS, self.homeStarter2ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.homeStarter3WS, self.homeStarter3ProjectedWS, self.homeStarter3LastWS, self.homeStarter3Last2WS, self.homeStarter3Last3WS, self.homeStarter3TotalLast3WS, self.homeStarter3ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.homeStarter4WS, self.homeStarter4ProjectedWS, self.homeStarter4LastWS, self.homeStarter4Last2WS, self.homeStarter4Last3WS, self.homeStarter4TotalLast3WS, self.homeStarter4ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.homeStarter5WS, self.homeStarter5ProjectedWS, self.homeStarter5LastWS, self.homeStarter5Last2WS, self.homeStarter5Last3WS, self.homeStarter5TotalLast3WS, self.homeStarter5ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.awayStarter1, self.awayStarter2, self.awayStarter3, self.awayStarter4, self.awayStarter5 = "", "", "", "", ""
        self.awayStarter1WS, self.awayStarter1ProjectedWS, self.awayStarter1LastWS, self.awayStarter1Last2WS, self.awayStarter1Last3WS, self.awayStarter1TotalLast3WS, self.awayStarter1ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.awayStarter2WS, self.awayStarter2ProjectedWS, self.awayStarter2LastWS, self.awayStarter2Last2WS, self.awayStarter2Last3WS, self.awayStarter2TotalLast3WS, self.awayStarter2ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.awayStarter3WS, self.awayStarter3ProjectedWS, self.awayStarter3LastWS, self.awayStarter3Last2WS, self.awayStarter3Last3WS, self.awayStarter3TotalLast3WS, self.awayStarter3ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.awayStarter4WS, self.awayStarter4ProjectedWS, self.awayStarter4LastWS, self.awayStarter4Last2WS, self.awayStarter4Last3WS, self.awayStarter4TotalLast3WS, self.awayStarter4ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0
        self.awayStarter5WS, self.awayStarter5ProjectedWS, self.awayStarter5LastWS, self.awayStarter5Last2WS, self.awayStarter5Last3WS, self.awayStarter5TotalLast3WS, self.awayStarter5ProjectedWSAdj = 0, 0, 0, 0, 0, 0, 0

    def fillData(self, gameStarters):
        self.homeStarter1 = gameStarters.homeStarter1
        starter = playerSeasonMap[self.homeStarter1][self.season]
        self.homeStarter1WS, self.homeStarter1ProjectedWS, self.homeStarter1LastWS, self.homeStarter1Last2WS, self.homeStarter1Last3WS, self.homeStarter1TotalLast3WS, self.homeStarter1ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.homeStarter2 = gameStarters.homeStarter2
        starter = playerSeasonMap[self.homeStarter2][self.season]
        self.homeStarter2WS, self.homeStarter2ProjectedWS, self.homeStarter2LastWS, self.homeStarter2Last2WS, self.homeStarter2Last3WS, self.homeStarter2TotalLast3WS, self.homeStarter2ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.homeStarter3 = gameStarters.homeStarter3
        starter = playerSeasonMap[self.homeStarter3][self.season]
        self.homeStarter3WS, self.homeStarter3ProjectedWS, self.homeStarter3LastWS, self.homeStarter3Last2WS, self.homeStarter3Last3WS, self.homeStarter3TotalLast3WS, self.homeStarter3ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj
        
        self.homeStarter4 = gameStarters.homeStarter4
        starter = playerSeasonMap[self.homeStarter4][self.season]
        self.homeStarter4WS, self.homeStarter4ProjectedWS, self.homeStarter4LastWS, self.homeStarter4Last2WS, self.homeStarter4Last3WS, self.homeStarter4TotalLast3WS, self.homeStarter4ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.homeStarter5 = gameStarters.homeStarter5
        starter = playerSeasonMap[self.homeStarter5][self.season]
        self.homeStarter5WS, self.homeStarter5ProjectedWS, self.homeStarter5LastWS, self.homeStarter5Last2WS, self.homeStarter5Last3WS, self.homeStarter5TotalLast3WS, self.homeStarter5ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.awayStarter1 = gameStarters.awayStarter1
        starter = playerSeasonMap[self.awayStarter1][self.season]
        self.awayStarter1WS, self.awayStarter1ProjectedWS, self.awayStarter1LastWS, self.awayStarter1Last2WS, self.awayStarter1Last3WS, self.awayStarter1TotalLast3WS, self.awayStarter1ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.awayStarter2 = gameStarters.awayStarter2
        starter = playerSeasonMap[self.awayStarter2][self.season]
        self.awayStarter2WS, self.awayStarter2ProjectedWS, self.awayStarter2LastWS, self.awayStarter2Last2WS, self.awayStarter2Last3WS, self.awayStarter2TotalLast3WS, self.awayStarter2ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.awayStarter3 = gameStarters.awayStarter3
        starter = playerSeasonMap[self.awayStarter3][self.season]
        self.awayStarter3WS, self.awayStarter3ProjectedWS, self.awayStarter3LastWS, self.awayStarter3Last2WS, self.awayStarter3Last3WS, self.awayStarter3TotalLast3WS, self.awayStarter3ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.awayStarter4 = gameStarters.awayStarter4
        starter = playerSeasonMap[self.awayStarter4][self.season]
        self.awayStarter4WS, self.awayStarter4ProjectedWS, self.awayStarter4LastWS, self.awayStarter4Last2WS, self.awayStarter4Last3WS, self.awayStarter4TotalLast3WS, self.awayStarter4ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

        self.awayStarter5 = gameStarters.awayStarter5
        starter = playerSeasonMap[self.awayStarter5][self.season]
        self.awayStarter5WS, self.awayStarter5ProjectedWS, self.awayStarter5LastWS, self.awayStarter5Last2WS, self.awayStarter5Last3WS, self.awayStarter5TotalLast3WS, self.awayStarter5ProjectedWSAdj = starter.winShares, starter.projectedWS, starter.lastWS, starter.last2WS, starter.last3WS, starter.totalLast3WS, starter.projectedWSAdj

def getId(link):
    return link[11:link.index(".")]

with open("nba-game-starters-2007-17.csv") as file:
    reader = csv.reader(file)
    for row in reader:
        if row[0] == "Id":
            continue
        gameStarters[row[0]] = GameStarters(row[0], getId(row[1]), getId(row[2]), getId(row[3]), getId(row[4]), getId(row[5]), getId(row[6]), getId(row[7]), getId(row[8]), getId(row[9]), getId(row[10]))

for year in years:
    with open("nba-games-" + str(year) + "-" + str(year + 1)[2:] + ".csv") as file:
        reader = csv.reader(file)
        for row in reader:
            if row[0] == "Id":
                continue
            game = GameInfo(row)
            game.fillData(gameStarters[game.gameId])
            gameInfo.append(game)

def getCsvRow(gameInfo):
    return [gameInfo.gameId, gameInfo.date, gameInfo.season, gameInfo.homeTeam, gameInfo.homeScore, gameInfo.homeEloPre, gameInfo.homeEloPost, gameInfo.homeDaysRest, gameInfo.homeFourthInFiveDays, gameInfo.homeFifthInSevenDays, gameInfo.homeDistance, gameInfo.homeOdds, gameInfo.homeProb, gameInfo.homeLogit, gameInfo.awayTeam, gameInfo.awayScore, gameInfo.awayEloPre, gameInfo.awayEloPost, gameInfo.awayDaysRest, gameInfo.awayFourthInFiveDays, gameInfo.awayFifthInSevenDays, gameInfo.awayDistance, gameInfo.awayOdds, gameInfo.eloDiff, gameInfo.isHomeVictory, gameInfo.homeStarter1, gameInfo.homeStarter1WS, gameInfo.homeStarter1ProjectedWS, gameInfo.homeStarter1ProjectedWSAdj, gameInfo.homeStarter1LastWS, gameInfo.homeStarter1Last2WS, gameInfo.homeStarter1Last3WS, gameInfo.homeStarter1TotalLast3WS, gameInfo.homeStarter2, gameInfo.homeStarter2WS, gameInfo.homeStarter2ProjectedWS, gameInfo.homeStarter2ProjectedWSAdj, gameInfo.homeStarter2LastWS, gameInfo.homeStarter2Last2WS, gameInfo.homeStarter2Last3WS, gameInfo.homeStarter2TotalLast3WS, gameInfo.homeStarter3, gameInfo.homeStarter3WS, gameInfo.homeStarter3ProjectedWS, gameInfo.homeStarter3ProjectedWSAdj, gameInfo.homeStarter3LastWS, gameInfo.homeStarter3Last2WS, gameInfo.homeStarter3Last3WS, gameInfo.homeStarter3TotalLast3WS, gameInfo.homeStarter4, gameInfo.homeStarter4WS, gameInfo.homeStarter4ProjectedWS, gameInfo.homeStarter4ProjectedWSAdj, gameInfo.homeStarter4LastWS, gameInfo.homeStarter4Last2WS, gameInfo.homeStarter4Last3WS, gameInfo.homeStarter4TotalLast3WS, gameInfo.homeStarter5, gameInfo.homeStarter5WS, gameInfo.homeStarter5ProjectedWS, gameInfo.homeStarter5ProjectedWSAdj, gameInfo.homeStarter5LastWS, gameInfo.homeStarter5Last2WS, gameInfo.homeStarter5Last3WS, gameInfo.homeStarter5TotalLast3WS, gameInfo.awayStarter1, gameInfo.awayStarter1WS, gameInfo.awayStarter1ProjectedWS, gameInfo.awayStarter1ProjectedWSAdj, gameInfo.awayStarter1LastWS, gameInfo.awayStarter1Last2WS, gameInfo.awayStarter1Last3WS, gameInfo.awayStarter1TotalLast3WS, gameInfo.awayStarter2, gameInfo.awayStarter2WS, gameInfo.awayStarter2ProjectedWS, gameInfo.awayStarter2ProjectedWSAdj, gameInfo.awayStarter2LastWS, gameInfo.awayStarter2Last2WS, gameInfo.awayStarter2Last3WS, gameInfo.awayStarter2TotalLast3WS, gameInfo.awayStarter3, gameInfo.awayStarter3WS, gameInfo.awayStarter3ProjectedWS, gameInfo.awayStarter3ProjectedWSAdj, gameInfo.awayStarter3LastWS, gameInfo.awayStarter3Last2WS, gameInfo.awayStarter3Last3WS, gameInfo.awayStarter3TotalLast3WS, gameInfo.awayStarter4, gameInfo.awayStarter4WS, gameInfo.awayStarter4ProjectedWS, gameInfo.awayStarter4ProjectedWSAdj, gameInfo.awayStarter4LastWS, gameInfo.awayStarter4Last2WS, gameInfo.awayStarter4Last3WS, gameInfo.awayStarter4TotalLast3WS, gameInfo.awayStarter5, gameInfo.awayStarter5WS, gameInfo.awayStarter5ProjectedWS, gameInfo.awayStarter5ProjectedWSAdj, gameInfo.awayStarter5LastWS, gameInfo.awayStarter5Last2WS, gameInfo.awayStarter5Last3WS, gameInfo.awayStarter5TotalLast3WS]

with open("nba-games-2007-17-full.csv", "w", newline="") as file:
    writer = csv.writer(file, quoting=csv.QUOTE_ALL)
    writer.writerow(["gameId", "date", "season", "homeTeam", "homeScore", "homeEloPre", "homeEloPost", "homeDaysRest", "homeFourthInFiveDays", "homeFifthInSevenDays", "homeDistance", "homeOdds", "homeProb", "homeLogit", "awayTeam", "awayScore", "awayEloPre", "awayEloPost", "awayDaysRest", "awayFourthInFiveDays", "awayFifthInSevenDays", "awayDistance", "awayOdds", "eloDiff", "isHomeVictory", "homeStarter1", "homeStarter1WS", "homeStarter1ProjectedWS", "homeStarter1ProjectedWSAdj", "homeStarter1LastWS", "homeStarter1Last2WS", "homeStarter1Last3WS", "homeStarter1TotalLast3WS", "homeStarter2", "homeStarter2WS", "homeStarter2ProjectedWS", "homeStarter2ProjectedWSAdj", "homeStarter2LastWS", "homeStarter2Last2WS", "homeStarter2Last3WS", "homeStarter2TotalLast3WS", "homeStarter3", "homeStarter3WS", "homeStarter3ProjectedWS", "homeStarter3ProjectedWSAdj", "homeStarter3LastWS", "homeStarter3Last2WS", "homeStarter3Last3WS", "homeStarter3TotalLast3WS", "homeStarter4", "homeStarter4WS", "homeStarter4ProjectedWS", "homeStarter4ProjectedWSAdj", "homeStarter4LastWS", "homeStarter4Last2WS", "homeStarter4Last3WS", "homeStarter4TotalLast3WS", "homeStarter5", "homeStarter5WS", "homeStarter5ProjectedWS", "homeStarter5ProjectedWSAdj", "homeStarter5LastWS", "homeStarter5Last2WS", "homeStarter5Last3WS", "homeStarter5TotalLast3WS", "awayStarter1", "awayStarter1WS", "awayStarter1ProjectedWS", "awayStarter1ProjectedWSAdj", "awayStarter1LastWS", "awayStarter1Last2WS", "awayStarter1Last3WS", "awayStarter1TotalLast3WS", "awayStarter2", "awayStarter2WS", "awayStarter2ProjectedWS", "awayStarter2ProjectedWSAdj", "awayStarter2LastWS", "awayStarter2Last2WS", "awayStarter2Last3WS", "awayStarter2TotalLast3WS", "awayStarter3", "awayStarter3WS", "awayStarter3ProjectedWS", "awayStarter3ProjectedWSAdj", "awayStarter3LastWS", "awayStarter3Last2WS", "awayStarter3Last3WS", "awayStarter3TotalLast3WS", "awayStarter4", "awayStarter4WS", "awayStarter4ProjectedWS", "awayStarter4ProjectedWSAdj", "awayStarter4LastWS", "awayStarter4Last2WS", "awayStarter4Last3WS", "awayStarter4TotalLast3WS", "awayStarter5", "awayStarter5WS", "awayStarter5ProjectedWS", "awayStarter5ProjectedWSAdj", "awayStarter5LastWS", "awayStarter5Last2WS", "awayStarter5Last3WS", "awayStarter5TotalLast3WS"])
    writer.writerows(list(map(getCsvRow, gameInfo)))
