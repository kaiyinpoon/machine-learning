import csv
import datetime
import math

class Game:
    def __init__(self, date, season, homeName, homeScore, homeOdds, awayName, awayScore, awayOdds):
        self.id = str(date) + "-" + homeName + "-" + awayName
        self.date = date
        self.season = season
        self.homeName, self.awayName = homeName, awayName
        self.homeOdds, self.awayOdds = homeOdds, awayOdds
        self.homeDistance = teamDistance[teamName[teamLastLocation[homeName]]][teamName[homeName]]
        self.awayDistance = teamDistance[teamName[teamLastLocation[awayName]]][teamName[homeName]]
        self.isHomeVictory = True if homeScore > awayScore else False

        homeProb, awayProb = getImpliedProb(homeOdds), getImpliedProb(awayOdds)
        self.homeImpliedProb, self.awayImpliedProb = getNormalizedProb(homeProb, awayProb)
        self.homeLogit = getLogit(self.homeImpliedProb)
        
        self.homeDaysRest = min(3, (date - teamSchedule[homeName][-1]).days - 1)
        self.awayDaysRest = min(3, (date - teamSchedule[awayName][-1]).days - 1)
        self.homeFourthInFiveDays = isFourthInFiveDays(homeName, teamSchedule[homeName], date)
        self.awayFourthInFiveDays = isFourthInFiveDays(awayName, teamSchedule[awayName], date)
        self.homeFifthInSevenDays = isFifthInSevenDays(homeName, teamSchedule[homeName], date)
        self.awayFifthInSevenDays = isFifthInSevenDays(awayName, teamSchedule[awayName], date)
        teamSchedule[homeName].append(date)
        teamSchedule[awayName].append(date)

        self.homeScore, self.awayScore = homeScore, awayScore
        self.homeEloPre, self.awayEloPre = teamElo[homeName], teamElo[awayName]
        self.eloDiff = round(self.homeEloPre + 100 - self.awayEloPre, 4)
        delta = getEloDelta(homeScore, awayScore, self.homeEloPre, self.awayEloPre)
        self.homeEloPost = round(self.homeEloPre + delta if self.isHomeVictory else self.homeEloPre - delta, 4)
        self.awayEloPost = round(self.awayEloPre - delta if self.isHomeVictory else self.awayEloPre + delta, 4)
        teamElo[homeName] = self.homeEloPost
        teamElo[awayName] = self.awayEloPost

        teamLastLocation[homeName] = homeName
        teamLastLocation[awayName] = homeName

def getCsvHeader():
    return ["Id", "Date", "Season", "HomeTeam", "HomeScore", "HomeEloPre", "HomeEloPost", "HomeDaysRest", "Home4thIn5", "Home5thIn7", "HomeDistance", "HomeOdds", "HomeProb", "HomeLogit", "AwayTeam", "AwayScore", "AwayEloPre", "AwayEloPost", "AwayDaysRest", "Away4thIn5", "Away5thIn7", "AwayDistance", "AwayOdds", "EloDiff", "IsHomeVictory"]

def getCsvRow(game):
    return [game.id, game.date, game.season, game.homeName, game.homeScore, game.homeEloPre, game.homeEloPost, game.homeDaysRest, game.homeFourthInFiveDays, game.homeFifthInSevenDays, game.homeDistance, game.homeOdds, game.homeImpliedProb, game.homeLogit, game.awayName, game.awayScore, game.awayEloPre, game.awayEloPost, game.awayDaysRest, game.awayFourthInFiveDays, game.awayFifthInSevenDays, game.awayDistance, game.awayOdds, game.eloDiff, game.isHomeVictory]

def getImpliedProb(odds):
    if odds == "NA":
        return "NA"
    elif odds > 0:
        return 100 / (odds + 100)
    return -odds / (-odds + 100)

def getNormalizedProb(homeProb, awayProb):
    if homeProb == "NA":
        return ("NA", "NA")
    return (round(homeProb / (homeProb + awayProb), 4), round(awayProb / (homeProb + awayProb), 4))

def getLogit(prob):
    if prob == "NA":
        return "NA"
    return round(math.log(1 / prob - 1), 4)

""" Formula borrowed from FiveThirtyEight """
def getEloDelta(homeScore, awayScore, homeEloPre, awayEloPre):
    return 20 * getMOVFactor(homeScore, awayScore, homeEloPre, awayEloPre) * getProbFactor(homeScore, awayScore, homeEloPre, awayEloPre)

def getProbFactor(homeScore, awayScore, homeEloPre, awayEloPre):
    if homeScore > awayScore:
        return 1 - 1 / (1 + 10 ** ((homeEloPre - awayEloPre + 100) / -400))
    return 1 / (1 + 10 ** ((homeEloPre - awayEloPre + 100) / -400))

def getMOVFactor(homeScore, awayScore, homeEloPre, awayEloPre):
    if homeScore > awayScore:
        return ((homeScore - awayScore + 3) ** .8) / (7.5 + .006 * (homeEloPre - awayEloPre + 100))
    return ((awayScore - homeScore + 3) ** .8) / (7.5 + .006 * (awayEloPre - homeEloPre - 100))

def getStartOfSeasonElo(elo):
    return round(.75 * elo + .25 * 1505, 4)

def isFourthInFiveDays(teamName, schedule, today):
    if len(schedule) < 3:
        return False
    return (today - schedule[-3]).days <= 4

def isFifthInSevenDays(teamName, schedule, today):
    if len(schedule) < 4:
        return False
    return (today - schedule[-4]).days <= 6

teamName = dict()
with open("team-map.csv") as teamMapFile:
    teamMapReader = csv.reader(teamMapFile)
    for row in teamMapReader:
        teamName[row[0]] = row[1]
        if row[0] != "Charlotte Bobcats" and row[0] != "New Orleans Hornets":
            teamName[row[1]] = row[0]

teamElo = dict()
teamSchedule = dict()
teamLastLocation = dict()
with open("nba-elo-2007-08.csv") as teamEloFile:
    teamEloReader = csv.reader(teamEloFile)
    for row in teamEloReader:
        teamElo[row[0]] = int(row[1])
        teamSchedule[row[0]] = [datetime.date(2006, 1, 1)]
        teamLastLocation[row[0]] = row[0]

teamDistance = dict()
with open("distance.csv") as distanceFile:
    distanceReader = csv.reader(distanceFile)
    i = 0
    teams = []
    for row in distanceReader:
        if i == 0:
            teams = row[1:]
            for team in teams:
                teamDistance[team] = dict()
        else:
            currentTeam = teamDistance[row[0]]
            for j in range(1, 33):
                currentTeam[teams[j - 1]] = int(row[j])
        i += 1

years = [x for x in range(2007, 2017)]
for year in years:
    games = []
    with open("nba-odds-" + str(year) + "-" + str(year + 1)[-2:] + ".csv") as oddsFile:
        oddsReader = csv.reader(oddsFile)
        i = 0
        for row in oddsReader:
            if row[2] == "H":
                homeName, homeScore, homeOdds = row[3], int(row[8]), "NA" if row[11] == "NL" else int(row[11])
            elif row[2] == "V":
                awayName, awayScore, awayOdds = row[3], int(row[8]), "NA" if row[11] == "NL" else int(row[11])
            else:
                continue
            if i % 2 == 1:
                mm, dd = int(row[0][:-2]), int(row[0][-2:])
                yyyy = year if len(row[0]) == 4 else year + 1
                date = datetime.date(yyyy, mm, dd)
                games.append(Game(date, year, homeName, homeScore, homeOdds, awayName, awayScore, awayOdds))
            i += 1
    with open("nba-games-" + str(year) + "-" + str(year + 1)[-2:] + ".csv", "w", newline="") as gamesFile:
        gameWriter = csv.writer(gamesFile, quoting=csv.QUOTE_ALL)
        gameWriter.writerow(getCsvHeader())
        gameWriter.writerows(list(map(getCsvRow, games)))
    for team in teamElo:
        teamElo[team] = getStartOfSeasonElo(teamElo[team])
        teamSchedule[team] = [teamSchedule[team][-1]]
        teamLastLocation[team] = team
    """ Seattle moved to Oklahoma City after 2007-08 season """
    if year == 2007:
        teamElo["OklahomaCity"] = teamElo["Seattle"]
    """ New Jersey moved to Brooklyn after 2011-12 season """
    if year == 2011:
        teamElo["Brooklyn"] = teamElo["NewJersey"]
