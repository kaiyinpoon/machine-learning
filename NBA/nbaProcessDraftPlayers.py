import nbaPlayerWSDataMining as nba
import urllib.request
import csv

class Player:
    def __init__(self, uniqueId, name, link, pick):
        self.uniqueId, self.name, self.link, self.pick = uniqueId, name, link, pick

players = []
with open("nba-draft-1973-06.csv") as playerFile:
    playerReader = csv.reader(playerFile)
    i = 0
    for row in playerReader:
        if i == 0:
            i += 1
            continue
        players.append(Player(row[2], row[1], row[3], int(row[0])))

playerSeasons = []
for player in players:
    with urllib.request.urlopen("https://www.basketball-reference.com" + player.link) as url:
        p = nba.PlayerParser(player.uniqueId, player.name, player.pick)
        html = str(url.read())
        html = html.replace("<!--\\n   <div", "<div").replace("</div>\\n-->\\n</div", "</div></div>")
        p.feed(html)
        playerSeasons.extend(p.seasons)
        p.close()

currentPlayer = ""
lastSeason = 1
lastYear = 0
i = 0
while i < len(playerSeasons):
    playerSeason = playerSeasons[i]
    i += 1
    if currentPlayer != playerSeason.name:
        currentPlayer = playerSeason.name
        lastSeason = playerSeason.season
        lastYear = playerSeason.year
    if playerSeason.season > 1 and lastSeason + 1 != playerSeason.season:
        for j in range(playerSeason.season - lastSeason - 1):
            playerSeasons.insert(i - 1, nba.PlayerSeason(playerSeason.uniqueId, playerSeason.name, lastYear + j + 1, lastSeason + j + 1, 0, playerSeason.pick))
            i += 1
    lastSeason = playerSeason.season
    lastYear = playerSeason.year

""" Filter out seasons 2007-08 and beyond because we are predicting results of those seasons"""
playerSeasons = list(filter(lambda x: x.year < 2007, playerSeasons))

def getCsvRow(playerSeason):
    return [playerSeason.uniqueId, playerSeason.name, playerSeason.season, playerSeason.winShares, playerSeason.year, playerSeason.pick]

with open("nba-draft-player-ws.csv", "w", newline="") as draftFile:
    draftWriter = csv.writer(draftFile, quoting=csv.QUOTE_ALL)
    draftWriter.writerow(["Id", "Name", "Season", "WS", "Year", "Pick"])
    draftWriter.writerows(list(map(getCsvRow, playerSeasons)))
