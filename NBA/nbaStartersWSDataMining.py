from html.parser import HTMLParser
import csv
import urllib.request
import re

class PlayerSeason:
    def __init__(self, uniqueId, year, season, winShares, pick = 0, lastWS = 0, last2WS = 0, last3WS = 0, totalLast3WS = 0, projectedWS = 0):
        self.uniqueId, self.year = uniqueId, year
        self.season, self.winShares, self.pick = season, winShares, pick
        self.lastWS, self.last2WS, self.last3WS = 0, 0, 0
        self.totalLast3WS, self.projectedWS, self.projectedWSAdj = 0, 0, 0

    def setPick(self, pick):
        self.pick = pick

    def setPastWS(self, lastWS, last2WS, last3WS):
        self.lastWS, self.last2WS, self.last3WS = lastWS, last2WS, last3WS
        self.totalLast3WS = round(lastWS + last2WS + last3WS, 1)

    # WS = 2.0309 + 0.0071 * Season ^ 2 - 0.2633 * Season - 0.0109 * Pick + 0.6241 * LastWS + 0.1555 * LAST2WS + 0.0630 * LAST3WS
    def setProjectedWS(self):
        self.projectedWS = round(2.0309 + 0.0071 * (self.season ** 2) - 0.2633 * self.season - 0.0109 * int(self.pick) + 0.6241 * self.lastWS + 0.1555 * self.last2WS + 0.063 * self.last3WS, 1)

    # Rookies: WS = 1.9642 - 0.0189 * Pick
    # Second year: WS = 1.4554 - 0.0144 * Pick + 0.8611 * Last_WS
    # Third year: WS = 0.8937 - 0.0077 * Pick + 0.7268 * Last_WS + 0.1817 * Last_2_WS
    # Fourth year & beyond: WS = 1.7984 + 0.0062 * Season ^ 2 - 0.2492 * Season - 0.0047 * Pick + 0.5469 * Last_WS + 0.1954 * Last_2_WS + 0.1013 * Last_3_WS
    def setProjectedWSAdj(self):
        if self.season == 1:
            self.projectedWSAdj = round(1.9642 - 0.0189 * int(self.pick), 1)
        elif self.season == 2:
            self.projectedWSAdj = round(1.4554 - 0.0144 * int(self.pick) + 0.8611 * self.lastWS, 1)
        elif self.season == 3:
            self.projectedWSAdj = round(0.8937 - 0.0077 * int(self.pick) + 0.7268 * self.lastWS + 0.1817 * self.last2WS, 1)
        else:
            self.projectedWSAdj = round(1.7984 + 0.0062 * (self.season ** 2) - 0.2492 * self.season - 0.0047 * int(self.pick) + 0.5469 * self.lastWS + 0.1954 * self.last2WS + 0.1013 * self.last3WS, 1)

class PlayerParser(HTMLParser):
    def __init__(self, uniqueId):
        HTMLParser.__init__(self)
        self.uniqueId = uniqueId
        self.advancedTag, self.partialTag, self.yearTag, self.winSharesTag = False, False, False, False
        self.season, self.firstYear, self.year, self.winShares = 0, 0, 0, 0
        self.seasons = []

    def handle_starttag(self, tag, attributes):
        if tag == "span":
            for name, value in attributes:
                if name == "id" and value == "advanced_link":
                    self.advancedTag = True
                elif name == "id" and value != "advanced_link":
                    self.advancedTag = False
        if tag == "tfoot":
            self.advancedTag = False
        if tag == "tr" and self.advancedTag:
            for name, value in attributes:
                if name == "class" and value == "full_table":
                    self.yearTag = True
                    self.partialTag = False
                if name == "class" and "partial_table" in value:
                    self.partialTag = True
        if tag == "td" and self.advancedTag:
            for name, value in attributes:
                if name == "data-stat" and value == "ws":
                    self.winSharesTag = True
                else:
                    self.winSharesTag = False

    def handle_data(self, data):
        if self.yearTag:
            self.yearTag = False
            if self.firstYear == 0:
                self.firstYear = int(data[:4])
            self.year = int(data[:4])
        if self.winSharesTag:
            self.winShares = round(float(data), 1)
            if self.advancedTag and not self.partialTag:
                self.seasons.append(PlayerSeason(self.uniqueId, self.year, self.year - self.firstYear + 1, self.winShares))

links = []
with open("nba-starters-2007-17.csv") as file:
    reader = csv.reader(file)
    for row in reader:
        if row[0] == "Link":
            continue
        links.append(row[0])
ids = list(map(lambda x: x[11:x.index(".")], links))

playerSeasons = []
pattern = "[0-9]+"
for i in range(len(links)):
    link = links[i]
    playerId = ids[i]
    seasons = []
    with urllib.request.urlopen("https://www.basketball-reference.com" + link) as url:
        p = PlayerParser(playerId)
        html = str(url.read())
        html = html.replace("<!--\\n   <div", "<div").replace("</div>\\n-->\\n</div", "</div></div>")
        p.feed(html)
        seasons.extend(p.seasons)
        p.close()
    try:
        index = html.index("overall)")
        pick = re.search(pattern, html[index - 10:index]).group()
    except ValueError:
        pick = 61
    for season in seasons:
        season.setPick(pick)
    playerSeasons.extend(seasons)

currentPlayer = ""
lastSeason = 1
lastYear = 0
i = 0
while i < len(playerSeasons):
    playerSeason = playerSeasons[i]
    i += 1
    if currentPlayer != playerSeason.uniqueId:
        currentPlayer = playerSeason.uniqueId
        lastSeason = playerSeason.season
        lastYear = playerSeason.year
    if playerSeason.season > 1 and lastSeason + 1 != playerSeason.season:
        for j in range(playerSeason.season - lastSeason - 1):
            playerSeasons.insert(i - 1, PlayerSeason(playerSeason.uniqueId, lastYear + j + 1, lastSeason + j + 1, 0, playerSeason.pick))
            i += 1
    lastSeason = playerSeason.season
    lastYear = playerSeason.year

player = ""
lastWS, last2WS, last3WS = 0, 0, 0
for playerSeason in playerSeasons:
    if playerSeason.uniqueId != player:
        player = playerSeason.uniqueId
        lastWS, last2WS, last3WS = 0, 0, 0
    playerSeason.setPastWS(lastWS, last2WS, last3WS)
    playerSeason.setProjectedWS()
    playerSeason.setProjectedWSAdj()
    lastWS, last2WS, last3WS = round(float(playerSeason.winShares), 1), lastWS, last2WS
playerSeasons = list(filter(lambda x: x.year >= 2007, playerSeasons))

def getCsvRow(playerSeason):
    return [playerSeason.uniqueId, playerSeason.year, playerSeason.season, playerSeason.winShares, playerSeason.pick, playerSeason.lastWS, playerSeason.last2WS, playerSeason.last3WS, playerSeason.totalLast3WS, playerSeason.projectedWS, playerSeason.projectedWSAdj]

with open("nba-starter-ws-full.csv", "w", newline="") as file:
    writer = csv.writer(file, quoting=csv.QUOTE_ALL)
    writer.writerow(["Id", "Year", "Season", "WS", "Pick", "Last WS", "Last 2 WS", "Last 3 WS", "Total Last 3 WS", "Projected WS", "Projected WS Adj"])
    writer.writerows(list(map(getCsvRow, playerSeasons)))
