import math
import csv

teamDistance = dict()
with open("../distance.csv") as distanceFile:
    distanceReader = csv.reader(distanceFile)
    i = 0
    teams = []
    for row in distanceReader:
        if i == 0:
            teams = row[1:]
            for team in teams:
                teamDistance[team] = dict()
        else:
            currentTeam = teamDistance[row[0]]
            for j in range(1, 33):
                currentTeam[teams[j - 1]] = int(row[j])
        i += 1

teamMap = dict()
with open("team-map-2.csv") as file:
    reader = csv.reader(file)
    for row in reader:
        teamMap[row[1]] = row[0]

def dist(team1, team2):
    return teamDistance[teamMap[team1]][teamMap[team2]]

lastLocation = dict()
with open("nba-results-2017-18.csv") as file:
    reader = csv.reader(file)
    for row in reader:
        lastLocation[row[1]] = row[1]
        lastLocation[row[5]] = row[1]

teamWS = dict()
with open("starters.csv") as file:
    reader = csv.reader(file)
    i = 0
    team = ""
    starterWS = 0
    for row in reader:
        if i % 6 == 0:
            if i > 0:
                teamWS[team] = starterWS
                starterWS = 0
            team = row[0]
        else:
            starterWS += round(float(row[1]) + float(row[2]) + float(row[3]), 1)
        i += 1
    teamWS[team] = starterWS

def ws(team):
    return teamWS[team]

def payout(odds):
    if odds > 0:
        return odds + 100
    return 100 * ((-odds) + 100) / (-odds)

def getImpliedProb(odds):
    if odds > 0:
        return 100 / (odds + 100)
    return -odds / (-odds + 100)

def getNormalizedProb(homeProb, awayProb):
    return (round(homeProb / (homeProb + awayProb), 4), round(awayProb / (homeProb + awayProb), 4))

def getLogit(prob):
    return round(math.log(1 / prob - 1), 4)

def defaultBetFunc(EV):
    return 8 * (EV ** 1.5)

""" Returns home win probability, EV of betting $100 home, EV of betting $100 away, and wager """
def ev(homeTeam, homeOdds, awayTeam, awayOdds, homeFourthInFive = False, awayFourthInFive = False, betFunc = None):
    if betFunc is None:
        betFunc = defaultBetFunc
    homeElo, awayElo = elo(homeTeam), elo(awayTeam)
    homeStartersLast3WS, awayStartersLast3WS = ws(homeTeam), ws(awayTeam)
    homeWin, awayWin = payout(homeOdds), payout(awayOdds)
    homeProb, awayProb = getImpliedProb(homeOdds), getImpliedProb(awayOdds)
    homeImpliedProb, awayImpliedProb = getNormalizedProb(homeProb, awayProb)
    homeLogit = getLogit(homeImpliedProb)
    eloDiff = round(homeElo + 100 - awayElo, 1)
    startersLast3WSDiff = homeStartersLast3WS - awayStartersLast3WS
    homeDistance = dist(lastLocation[homeTeam], homeTeam)
    homeDistanceLog = math.log(max(1, homeDistance))
    logit = 0.0097976 - 0.8492286 * homeLogit+ 0.0006574 * eloDiff + 0.0026543 * startersLast3WSDiff - 0.0029030 * homeDistanceLog - 0.1858373 * (1 if homeFourthInFive else 0) + 0.1281951 * (1 if awayFourthInFive else 0)
    homeProbHat = 1 / (1 + math.exp(-logit))
    betHome = homeProbHat * homeWin - 100
    betAway = (1 - homeProbHat) * awayWin - 100
    if betHome > 0:
        bet = round(betFunc(betHome), 2)
        expectedWinnings = round(bet * betHome / 100, 2)
    elif betAway > 0:
        bet = round(betFunc(betAway), 2)
        expectedWinnings = round(bet * betAway / 100, 2)
    else:
        bet = 0
        expectedWinnings = 0
    print("Implied Prob", "Home Prob", "EV Home", "EV Away", "Wager", sep=", ")
    return (round(homeImpliedProb, 4), round(homeProbHat, 4), round(betHome, 4), round(betAway, 4), bet if bet == 0 else str(bet) + " on " + (homeTeam if betHome > betAway else awayTeam))

""" Formula borrowed from FiveThirtyEight """
def getEloDelta(homeScore, awayScore, homeEloPre, awayEloPre):
    return 20 * getMOVFactor(homeScore, awayScore, homeEloPre, awayEloPre) * getProbFactor(homeScore, awayScore, homeEloPre, awayEloPre)

def getProbFactor(homeScore, awayScore, homeEloPre, awayEloPre):
    if homeScore > awayScore:
        return 1 - 1 / (1 + 10 ** ((homeEloPre - awayEloPre + 100) / -400))
    return 1 / (1 + 10 ** ((homeEloPre - awayEloPre + 100) / -400))

def getMOVFactor(homeScore, awayScore, homeEloPre, awayEloPre):
    if homeScore > awayScore:
        return ((homeScore - awayScore + 3) ** .8) / (7.5 + .006 * (homeEloPre - awayEloPre + 100))
    return ((awayScore - homeScore + 3) ** .8) / (7.5 + .006 * (awayEloPre - homeEloPre - 100))

games = []
class Game:
    def __init__(self, date, homeTeam, homeScore, homeEloPre, awayTeam, awayScore, awayEloPre):
        self.date = date
        self.homeTeam, self.homeScore, self.homeEloPre = homeTeam, homeScore, homeEloPre
        self.awayTeam, self.awayScore, self.awayEloPre = awayTeam, awayScore, awayEloPre
        self.isHomeVictory = True if homeScore > awayScore else False
        delta = getEloDelta(homeScore, awayScore, self.homeEloPre, self.awayEloPre)
        self.homeEloPost = round(self.homeEloPre + delta if self.isHomeVictory else self.homeEloPre - delta, 3)
        self.awayEloPost = round(self.awayEloPre - delta if self.isHomeVictory else self.awayEloPre + delta, 3)
        teamElo[self.homeTeam], teamElo[self.awayTeam] = self.homeEloPost, self.awayEloPost

teams = ["atl","bos","brk","cha","chi","cle","dal","den","det","gsw","hou","ind","lac","lal","mem","mia","mil","min","nop","nyk","okc","orl","phi","phx","por","sac","sas","tor","uta","was"]

def addGame(date, homeTeam, homeScore, awayTeam, awayScore):
    if homeTeam not in teams or awayTeam not in teams:
        raise ValueError("No such team exists")
    homeEloPre, awayEloPre = elo(homeTeam), elo(awayTeam)    
    games.append(Game(date, homeTeam, homeScore, homeEloPre, awayTeam, awayScore, awayEloPre))

def printGames():
    for game in games:
        print(game.date, game.homeTeam, game.homeScore, game.homeEloPre, game.homeEloPost, game.awayTeam, game.awayScore, game.awayEloPre, game.awayEloPost)

def writeGames():
    with open("nba-results-2017-18.csv", "a", newline="") as file:
        writer = csv.writer(file)
        for game in games:
            teamElo[game.homeTeam] = game.homeEloPost
            teamElo[game.awayTeam] = game.awayEloPost
            writer.writerow([game.date, game.homeTeam, game.homeScore, game.homeEloPre, game.homeEloPost, game.awayTeam, game.awayScore, game.awayEloPre, game.awayEloPost])
    with open("nba-elo-2017-18.csv", "w", newline="") as file:
        writer = csv.writer(file)
        for team in sorted(teamElo.keys()):
            writer.writerow([team, teamElo[team]])

teamElo = dict()
with open("nba-elo-2017-18.csv") as file:
    reader = csv.reader(file)
    for row in reader:
        teamElo[row[0]] = round(float(row[1]), 3)

def elo(team):
    return teamElo[team]
