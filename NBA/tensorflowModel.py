from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import urllib.request
import numpy as np
import tensorflow as tf
import collections
import csv
from tensorflow.python.platform import gfile

def payout(odds):
    if odds > 0:
        return odds + 100
    return 100 * ((-odds) + 100) / (-odds)

Dataset = collections.namedtuple('Dataset', ['data', 'target'])
NBA = "nba-games-2007-17-clean.csv"

n_samples = 12885
n_features = 9
training_size = 10308
test_size = 2577
test_home_odds = [0 for _ in range(test_size)]
test_away_odds = [0 for _ in range(test_size)]
i = 0
with open("nba-games-2007-17-full.csv") as f:
    reader = csv.reader(f)
    for row in reader:
        if i == 0:
            i += 1
            continue
        if row[11] == "NA":
            continue
        if i > training_size:
            test_home_odds[i - 1 - training_size] = int(row[11])
            test_away_odds[i - 1 - training_size] = int(row[22])
        i += 1

with gfile.Open(NBA) as csv_file:
    data_file = csv.reader(csv_file)
    header = next(data_file)
    training_data = np.zeros((training_size, n_features), dtype="f")
    test_data = np.zeros((test_size, n_features), dtype="f")
    all_data = np.zeros((n_samples, n_features), dtype="f")
    training_target = np.zeros((training_size,), dtype="?")
    test_target = np.zeros((test_size,), dtype="?")
    all_target = np.zeros((n_samples,), dtype="?")
    training_counter = 0
    test_counter = 0
    indices = sorted(np.random.choice(12885, training_size, replace=False))
    for i, row in enumerate(data_file):
        a = row.pop(8)
        all_target[i] = np.asarray(a == "TRUE", dtype="?")
        all_data[i] = np.asarray((float(row[0]), float(row[3]), int(row[4]), float(row[7]), float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12])), dtype="f")
        if training_counter < len(indices) and i == indices[training_counter]:
            training_target[training_counter] = np.asarray(a == "TRUE", dtype="?")
            training_data[training_counter] = np.asarray((float(row[0]), float(row[3]), int(row[4]), float(row[7]), float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12])), dtype="f")
            training_counter += 1
        else:
            test_target[test_counter] = np.asarray(a == "TRUE", dtype="?")
            test_data[test_counter] = np.asarray((float(row[0]), float(row[3]), int(row[4]), float(row[7]), float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12])), dtype="f")
            test_counter += 1
    training_data = Dataset(data=training_data, target=training_target)
    test_data = Dataset(data=test_data, target=test_target)
    all_data = Dataset(data=all_data, target=all_target)

homeDaysRest =  tf.feature_column.bucketized_column(tf.feature_column.numeric_column("f0"), boundaries = [0.5, 1.5, 2.5])
homeLogit = tf.feature_column.numeric_column("f1")
awayDaysRest = tf.feature_column.bucketized_column(tf.feature_column.numeric_column("f2"), boundaries = [0.5, 1.5, 2.5])
eloDiff = tf.feature_column.numeric_column("f3")
homeDistanceLog = tf.feature_column.numeric_column("f4")
awayDistanceLog = tf.feature_column.numeric_column("f5")
startersProjectedWSDiff = tf.feature_column.numeric_column("f6")
startersProjectedWSAdjDiff = tf.feature_column.numeric_column("f7")
startersLast3WSDiff = tf.feature_column.numeric_column("f8")

feature_columns = [homeDaysRest,homeLogit,awayDaysRest,eloDiff,homeDistanceLog,awayDistanceLog,startersProjectedWSDiff,startersProjectedWSAdjDiff,startersLast3WSDiff]
"""
for _ in range(10):
    with gfile.Open(NBA) as csv_file:
        data_file = csv.reader(csv_file)
        header = next(data_file)
        training_data = np.zeros((training_size, n_features), dtype="f")
        test_data = np.zeros((test_size, n_features), dtype="f")
        all_data = np.zeros((n_samples, n_features), dtype="f")
        training_target = np.zeros((training_size,), dtype="?")
        test_target = np.zeros((test_size,), dtype="?")
        all_target = np.zeros((n_samples,), dtype="?")
        training_counter = 0
        test_counter = 0
        indices = sorted(np.random.choice(12885, 10308, replace=False))
        for i, row in enumerate(data_file):
            a = row.pop(8)
            all_target[i] = np.asarray(a == "TRUE", dtype="?")
            all_data[i] = np.asarray((float(row[0]), float(row[3]), int(row[4]), float(row[7]), float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12])), dtype="f")
            if training_counter < len(indices) and i == indices[training_counter]:
                training_target[training_counter] = np.asarray(a == "TRUE", dtype="?")
                training_data[training_counter] = np.asarray((float(row[0]), float(row[3]), int(row[4]), float(row[7]), float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12])), dtype="f")
                training_counter += 1
            else:
                test_target[test_counter] = np.asarray(a == "TRUE", dtype="?")
                test_data[test_counter] = np.asarray((float(row[0]), float(row[3]), int(row[4]), float(row[7]), float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12])), dtype="f")
                test_counter += 1
        training_data = Dataset(data=training_data, target=training_target)
        test_data = Dataset(data=test_data, target=test_target)
        all_data = Dataset(data=all_data, target=all_target)

    classifier = tf.contrib.learn.DNNClassifier(feature_columns = feature_columns,
                                                hidden_units = [20, 20, 20, 20],
                                                optimizer=tf.train.ProximalAdagradOptimizer(
                                                        learning_rate=0.1,
                                                        l1_regularization_strength=0.001),
                                                n_classes = 2)

    train_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"f0": np.array(training_data.data)[:,0], "f1": np.array(training_data.data)[:,1], "f2": np.array(training_data.data)[:,2], "f3": np.array(training_data.data)[:,3], "f4": np.array(training_data.data)[:,4], "f5": np.array(training_data.data)[:,5], "f6": np.array(training_data.data)[:,6], "f7": np.array(training_data.data)[:,7], "f8": np.array(training_data.data)[:,8]},
            y=np.array(training_data.target),
            num_epochs=None,
            shuffle=True)
    classifier.fit(input_fn=train_input_fn, steps=2000)

    test_input_fn = tf.estimator.inputs.numpy_input_fn(
            x={"f0": np.array(test_data.data)[:,0], "f1": np.array(test_data.data)[:,1], "f2": np.array(test_data.data)[:,2], "f3": np.array(test_data.data)[:,3], "f4": np.array(test_data.data)[:,4], "f5": np.array(test_data.data)[:,5], "f6": np.array(test_data.data)[:,6], "f7": np.array(test_data.data)[:,7], "f8": np.array(test_data.data)[:,8]},
            y=np.array(test_data.target),
            num_epochs=1,
            shuffle=False)

    evluation = classifier.evaluate(input_fn=test_input_fn)
    print(evluation["accuracy"])

    pred = list(classifier.predict_proba(input_fn=test_input_fn))

    winHome = list(map(payout, test_home_odds))
    winAway = list(map(payout, test_away_odds))
    betHome = [p[1] * h - 100 for p, h in zip(pred, winHome)]
    betAway = [p[0] * a - 100 for p, a in zip(pred, winAway)]
    betHomeResult = [h if isHomeVictory else 0 for isHomeVictory, h in zip(test_data.target, winHome)]
    betAwayResult = [0 if isHomeVictory else a for isHomeVictory, a in zip(test_data.target, winAway)]
    betterBetEV = [max(betHome[i], betAway[i]) for i in range(len(betHome))]
    betterBetIsHome = [betHome[i] > betAway[i] for i in range(len(betHome))]
    betterBetActual = [((winHome[i] if betterBetIsHome[i] else winAway[i]) if test_data.target[i] == betterBetIsHome[i] else 0) - 100 for i in range(len(betHome))]
    wager = [betterBetEV[i] if betterBetEV[i] >= 5 else 0 for i in range(len(betterBetEV))]
    wagerActual = [betterBetActual[i] * wager[i] / 100 for i in range(len(wager))]
    print(sum(wagerActual))
    print(sum(wager))
"""
classifier = tf.contrib.learn.DNNClassifier(feature_columns = feature_columns,
                                            hidden_units = [20, 20, 20, 20],
                                            optimizer=tf.train.ProximalAdagradOptimizer(
                                                    learning_rate=0.1,
                                                    l1_regularization_strength=0.001),
                                            n_classes = 2)

train_input_fn = tf.estimator.inputs.numpy_input_fn(
        x={"f0": np.array(all_data.data)[:,0], "f1": np.array(all_data.data)[:,1], "f2": np.array(all_data.data)[:,2], "f3": np.array(all_data.data)[:,3], "f4": np.array(all_data.data)[:,4], "f5": np.array(all_data.data)[:,5], "f6": np.array(all_data.data)[:,6], "f7": np.array(all_data.data)[:,7], "f8": np.array(all_data.data)[:,8]},
        y=np.array(all_data.target),
        num_epochs=None,
        shuffle=False)
classifier.fit(input_fn=train_input_fn, steps=2000)
