from html.parser import HTMLParser
import urllib.request
import csv
import re

teamName = dict()
with open("team-code.csv") as teamMapFile:
    teamMapReader = csv.reader(teamMapFile)
    for row in teamMapReader:
        teamName[row[0]] = row[1]

years = range(2007, 2017)
links = []
gameIds = []
for year in years:
    if year == 2013:
        teamName["NewOrleans"] = "NOP"
    if year == 2014:
        teamName["Charlotte"] = "CHO"
    with open("nba-games-" + str(year) + "-" + str(year + 1)[2:] + ".csv") as gamesFile:
        gameReader = csv.reader(gamesFile)
        for row in gameReader:
            if row[0] == "Id":
                continue
            links.append(row[1].replace("-", "") + "0" + teamName[row[3]])
            gameIds.append(row[0])

class GameStarters:
    def __init__(self, gameId, homeStarter1, homeStarter2, homeStarter3, homeStarter4, homeStarter5, awayStarter1, awayStarter2, awayStarter3, awayStarter4, awayStarter5):
        self.gameId, self.homeStarter1, self.homeStarter2 = gameId, homeStarter1, homeStarter2
        self.homeStarter3, self.homeStarter4, self.homeStarter5 = homeStarter3, homeStarter4, homeStarter5
        self.awayStarter1, self.awayStarter2, self.awayStarter3 = awayStarter1, awayStarter2, awayStarter3
        self.awayStarter4, self.awayStarter5 = awayStarter4, awayStarter5

gameStarters = []
allStarters = set()
for i in range(len(links)):
    link = links[i]
    gameId = gameIds[i]
    with urllib.request.urlopen("https://www.basketball-reference.com/boxscores/" + link + ".html") as url:
        html = str(url.read())
        players = [x.start() for x in re.finditer("/players/", html)]
        players = list(filter(lambda x: "html" in html[x:x + 50], players))
        uniqueIds = list(map(lambda x: html[x:x + html[x:x + 50].index("html") + 4], players))
        awayStarters = uniqueIds[:5]
        allStarters.update(awayStarters)
        awayReserves = 0
        for uniqueId in uniqueIds[5:]:
            if uniqueId in awayStarters:
                break
            awayReserves += 1
        c = 2 * (5 + awayReserves)
        homeStarters = uniqueIds[c:c + 5]
        allStarters.update(homeStarters)
        gameStarters.append(GameStarters(gameId, homeStarters[0], homeStarters[1], homeStarters[2], homeStarters[3], homeStarters[4], awayStarters[0], awayStarters[1], awayStarters[2], awayStarters[3], awayStarters[4]))

with open("nba-starters-2007-17.csv", "w", newline="") as file:
    writer = csv.writer(file, quoting=csv.QUOTE_ALL)
    writer.writerow(["Link"])
    writer.writerows(list(map(lambda x: [x], allStarters)))

def getCsvRow(gameStarters):
    return [gameStarters.gameId, gameStarters.homeStarter1, gameStarters.homeStarter2, gameStarters.homeStarter3, gameStarters.homeStarter4, gameStarters.homeStarter5, gameStarters.awayStarter1, gameStarters.awayStarter2, gameStarters.awayStarter3, gameStarters.awayStarter4, gameStarters.awayStarter5]

with open("nba-game-starters-2007-17.csv", "w", newline="") as file:
    writer = csv.writer(file, quoting=csv.QUOTE_ALL)
    writer.writerow(["Id", "HomeStarter1", "HomeStarter2", "HomeStarter3", "HomeStarter4", "HomeStarter5", "AwayStarter1", "AwayStarter2", "AwayStarter3", "AwayStarter4", "AwayStarter5"])
    writer.writerows(list(map(getCsvRow, gameStarters)))
