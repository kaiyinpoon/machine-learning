from html.parser import HTMLParser

class PlayerSeason:
    def __init__(self, uniqueId, name, year, season, winShares, pick):
        self.uniqueId, self.name, self.year = uniqueId, name, year
        self.season, self.winShares, self.pick = season, winShares, pick

class PlayerParser(HTMLParser):
    def __init__(self, uniqueId, name, pick):
        HTMLParser.__init__(self)
        self.uniqueId, self.name, self.pick = uniqueId, name, pick
        self.advancedTag, self.partialTag, self.yearTag, self.winSharesTag = False, False, False, False
        self.season = 0
        self.firstYear = 0
        self.year = 0
        self.winShares = 0
        self.seasons = []

    def handle_starttag(self, tag, attributes):
        if tag == "span":
            for name, value in attributes:
                if name == "id" and value == "advanced_link":
                    self.advancedTag = True
                elif name == "id" and value != "advanced_link":
                    self.advancedTag = False
        if tag == "tfoot":
            self.advancedTag = False
        if tag == "tr" and self.advancedTag:
            for name, value in attributes:
                if name == "class" and value == "full_table":
                    self.yearTag = True
                    self.partialTag = False
                if name == "class" and "partial_table" in value:
                    self.partialTag = True
        if tag == "td" and self.advancedTag:
            for name, value in attributes:
                if name == "data-stat" and value == "ws":
                    self.winSharesTag = True
                else:
                    self.winSharesTag = False

    def handle_data(self, data):
        if self.yearTag:
            self.yearTag = False
            if self.firstYear == 0:
                self.firstYear = int(data[:4])
            self.year = int(data[:4])
        if self.winSharesTag:
            self.winShares = data
            if self.year >= 1973 and self.advancedTag and not self.partialTag:
                self.seasons.append(PlayerSeason(self.uniqueId, self.name, self.year, self.year - self.firstYear + 1, self.winShares, self.pick))
