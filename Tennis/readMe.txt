List of tournaments included (completeness of results not guaranteed):
1) Pre open era grand slams (amateur)
	a) Wimbledon Championships (1877-1914, 1919-39, 1946-67)
	b) US National Championships (1881-1954, 1957-67)
	c) Australian/Australasian Championships (1905-15, 1919-40, 1946-67)
	d) French Championships (1925-39, 1947-67)
2) Open era grand slams
	a) Wimbledon (1968-present)
	b) US Open (1968-present)
	c) Australian Open (1968-present)
	d) French Open (1968-present)
3) Pre open era grand slams (professional)
	a) US Pro (1927-43, 1945-67)
	b) French Pro (1930-32, 1934-39, 1956, 1958-67)
	c) Wembley Pro (1934-35, 1937, 1939, 1949-53, 1956-67)
4) ILTF grand slams
	a) World Hard Court Championships (1912-14, 1920-24)
	b) World Covered Court Championships (1913, 1919-23)
5) Summer Olympics (1896-1912, 1920-24, 1988-present)
6) World Championship Tennis (1970-77, 1982-84)
7) Grand Prix/ATP World Tour (1970-present)
8) Selected Tournaments (1968-71)

Excludes qualifying.

Excludes matches if it does not use a first to 6 (either advantage or tiebreak sets) scoring system.

The advent of the open era was a hectic time. The NTL and WCT both signed prominent players and hosted tournaments. The results of those tournaments are absent from the ATP website. From 1970 (when NTL was purchased by WCT) onwards, the ATP website lists most WCT and Grand Prix tournaments. For the years 1968 and 1969, a tournament (other than the four grand slams) is included if
1) it is participated by at least 4 of the NTL (Rod Laver, Ken Rosewall, Pancho Gonzales, Andrés Gimeno, Fred Stolle, and Roy Emerson) and/or WCT (Dennis Ralston, John Newcombe, Tony Roche, Cliff Drysdale, Earl Buchholz, Niki Pilić, Roger Taylor, and Pierre Barthès) players, or
2) the tournamnet or its 1970 iteration is on the ATP website

For the years 1970 and 1971, a tournament is included if the 1968 and/or 1969 iterations are on the ATP website

Special mention
1933 French Pro
1936 Wembley Pro
1938 Wembley Pro

Unknown/wrong participants (list discreprencies)
missing.txt

Round robin
Unknown tournament dates

Choosing K-factor
http://fivethirtyeight.com/features/serena-williams-and-the-difference-between-all-time-great-and-greatest-of-all-time/#fn-3