from datetime import datetime
from training import runModel, getELogistic, clean
from urllib.request import Request, urlopen
from lxml import etree
import numpy as np
import pandas as pd
import re, os, csv

altnames = {
    "albertramosvinolas": "albertramos",
    "cristiangarin": "christiangarin"
}
skip = []

def payout(odds):
    if odds > 0:
        return odds
    return 100 * ((-odds) + 100) / (-odds) - 100

def ev(p1, l1, p2, l2):
    if p1 in skip or p2 in skip or players[p1][-1] < 50 or players[p2][-1] < 50:
        return [-1 for _ in range(6)]
    elo1, elo2 = players[p1][0], players[p2][0]
    prob1 = getELogistic(elo1 - elo2)
    prob2 = 1 - prob1
    ev1, ev2 = prob1 * payout(l1) - prob2 * 100, prob2 * payout(l2) - prob1 * 100
    return p1, round(ev1, 4), round(((payout(l1) - ev1) ** 2 * prob1 + (-100 - ev1) ** 2 * prob2), 4), p2, round(ev2, 4), round(((payout(l2) - ev2) ** 2 * prob2 + (-100 - ev2) ** 2 * prob1), 4)

def parse_match(div):
    try:
        first, second = div[0][1][1], div[0][1][2]
        p1 = clean("".join(first[1][0][0].text.split(", ")[::-1]))
        if p1 in altnames:
            p1 = altnames[p1]
        p2 = clean("".join(second[1][0][0].text.split(", ")[::-1]))
        if p2 in altnames:
            p2 = altnames[p2]
        l1 = int(first[4][0][0].text.split(" ")[0])
        l2 = int(second[4][0][0].text.split(" ")[0])
        return (p1, l1, p2, l2)
    except:
        return None

def get_matches(data):
    start = [s.start() for s in re.finditer('<div class="league', data)]
    end = start[1:] + [data.index('<div class="liveLinesPageDesc') - 10]
    sections = list(map(lambda i: data[start[i]:end[i]], range(len(start))))
    parser = etree.XMLParser(recover=True)
    matches = []
    for section in sections:
        tree = etree.fromstring(section, parser)
        if tree[0][0][0].text.startswith("ATP") and "QUALIF" not in tree[0][0][0].text:
            divs = list(filter(lambda x: 'class' in x.attrib and 'matchup' in x.attrib['class'], tree[1][1].getchildren()))
            for div in divs:
                match = parse_match(div)
                if match is not None:
                    matches.append(match)
    return matches

def get_request():
    req = Request("https://www.bookmaker.eu/live-lines/tennis")
    req.add_header('Accept-Encoding', 'none')
    req.add_header('Accept-Language', 'en-US,en;q=0.8')
    req.add_header('User-Agent', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11')
    req.add_header('Accept', 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8')
    req.add_header('Connection', 'keep-alive')
    req.add_header('Accept-Charset', 'ISO-8859-1,utf-8;q=0.7,*;q=0.3')
    return req

def get_current_bets():
    files = [f for f in os.listdir('../Bets') if "bets" in f][-2:]
    current_bets = []
    for f in files:
        with open('../Bets/' + f) as f:
            reader = csv.reader(f)
            for row in reader:
                current_bets.append((row[0], row[1]))
    return current_bets

def get_bets(from_file=False):
    current_bets = get_current_bets()
    bets = []
    if from_file:
        with open('../odds.csv') as f:
            reader = csv.reader(f)
            for match in reader:
                p1, z1, v1, p2, z2, v2 = ev(clean(match[0]), int(match[1]), clean(match[2]), int(match[3]))
                if (p1, p2) in current_bets or (p2, p1) in current_bets:
                    continue
                if z1 > 0:
                    bets.append([p1, p2, z1, v1, int(match[1])])
                elif z2 > 0:
                    bets.append([p2, p1, z2, v2, int(match[3])])
    else:
        file = urlopen(get_request())
        data = file.read().decode("utf-8")
        matches = get_matches(data)
        for match in matches:
            p1, z1, v1, p2, z2, v2 = ev(match[0], int(match[1]), match[2], int(match[3]))
            if (p1, p2) in current_bets or (p2, p1) in current_bets:
                continue
            if z1 > 0:
                bets.append([p1, p2, z1, v1, int(match[1])])
            elif z2 > 0:
                bets.append([p2, p1, z2, v2, int(match[3])])
    if len(bets) == 0:
        return pd.DataFrame()
    df = pd.DataFrame(bets, columns=["player", "opp", "ev", "variance", "odds"])
    z = np.array(df["ev"])
    S = np.diag(df["variance"])
    Sinv = np.linalg.inv(S)
    ones = np.array([1 for _ in range(len(z))])
    A = ones.T.dot(Sinv).dot(ones)
    B = ones.T.dot(Sinv).dot(z)
    C = z.T.dot(Sinv).dot(z)
    d = A * C - B ** 2
    w = Sinv.dot(ones) / A
    target_variance = 1600
    if (2 * B) ** 2 - 4 * A * (C - d * target_variance) > 0:
        target_mu = (2 * B + ((2 * B) ** 2 - 4 * A * (C - d * target_variance)) ** .5) / (2 * A)
    else:
        target_mu = B / A
        target_variance = (-1 * B ** 2 / A + C) / d
    l = (C - target_mu * B) / d
    g = (target_mu * A - B) / d
    target_w = l * A * w + g * Sinv.dot(z)
    stake = (target_mu * (40 / (target_variance ** .5)) * 2 * w).round(2)
    df2 = pd.DataFrame({"ev": [target_mu / 100 * sum(stake)], "var": [target_variance / 100 * sum(stake)]}, columns=["ev", "var"])
    df2.to_csv("../Bets/portfolio." + datetime.utcnow().strftime("%Y.%m.%d.%H.%M") + ".csv", header=False, index=False)
    df = pd.DataFrame({"player": df["player"], "opp": df["opp"], "odds": df["odds"], "bet": stake}, columns=["player", "opp", "odds", "bet"])
    df.to_csv("../Bets/bets." + datetime.utcnow().strftime("%Y.%m.%d.%H.%M") + ".csv", header=False, index=False)
    return df

if __name__ == "__main__":
    matchesElo, players, _, _ = runModel()
    get_bets()
