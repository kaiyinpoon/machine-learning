import pandas as pd
from datetime import datetime
import re

level_dict = {
    "G": "Grand Slam",
    "M": "ATP Master",
    "F": "ATP Finals",
    "D": "Davis Cup",
    "A": "Others",
    "C": "Others",
    "S": "Others"
}

round_dict = {
    "R128": "Round of 128",
    "R64": "Round of 64",
    "R32": "Round of 32",
    "R16": "Round of 16",
    "RR": "Round Robin",
    "QF": "Quarter-Finals",
    "SF": "Semi-Finals",
    "F": "Finals",
    "BR": "Olympic Bronze",
    "ER": "Elimination Round"
}

def exportMatches(year):
    matches = []
    link = "https://raw.githubusercontent.com/JeffSackmann/tennis_atp/master/atp_matches_" + str(year) + ".csv"
    df = pd.read_csv(link)
    df = df.sort_values(["tourney_date", "tourney_name", "match_num"], ascending=[True, True, False], kind='mergesort').reset_index(drop=True)
    df = df[["winner_name", "loser_name", "score", "tourney_name", "tourney_level", "tourney_date", "surface", "round"]]
    df["wId"] = [None] * len(df)
    df["lId"] = [None] * len(df)
    df["door"] = [None] * len(df)
    df["round"] = df["round"].apply(lambda x: x if pd.isna(x) else round_dict[x])
    df["tourney_level"] = df["tourney_level"].apply(lambda x: level_dict[x])
    df["tourney_date"] = df["tourney_date"].apply(lambda x: str(x)[:4] + "." + str(x)[4:6] + "." + str(x)[6:])
    df["score"] = df["score"].apply(lambda x: re.sub(r'\([0-9]+\)', '', str(x).replace('-', '')))
    df = df[["winner_name", "wId", "loser_name", "lId", "score", "tourney_name", "tourney_level", "tourney_date", "door", "surface", "round"]]
    df.to_csv("../Results/matches" + str(year) + ".csv", index=False, header=False)

if __name__ == "__main__":
    roundNames = set()
    for year in range(1970, datetime.utcnow().year + 1):
        print(year)
        exportMatches(year)
