from training import *

def erfcc(x):
    """Complementary error function."""
    z = abs(x)
    t = 1. / (1. + 0.5*z)
    r = t * exp(-z*z-1.26551223+t*(1.00002368+t*(.37409196+
    	t*(.09678418+t*(-.18628806+t*(.27886807+
    	t*(-1.13520398+t*(1.48851587+t*(-.82215223+
    	t*.17087277)))))))))
    if (x >= 0.):
    	return r
    else:
    	return 2. - r

def ncdf(x):
    return 1. - 0.5*erfcc(x/(2**0.5))

# Difference of +400 elo = 90% winning probability
def getENormal(diff, sd = 312):
    return round(ncdf(diff / sd), 4)

def seqGen(ll, ceiling = 1):
    result = []
    if len(ll) == 1:
        return [[x] for x in filter(lambda x: x <= ceiling, ll[0])]
    lst = ll[0]
    for val in lst:
        if val <= ceiling:
            for seq in seqGen(ll[1:], val):
                result.append([val] + seq)
    return result

def constantAlgorithmMaker(constants):
    def makeAlgorithm(constant):
        def getK(wElo, lElo, wMatch, lMatch):
            return (constant, constant)
        return Algorithm(getK, "c: " + str(constant))
    return list(map(makeAlgorithm, constants))

def linearMatchAlgorithmMaker(k1s, k2s, m1s, m2s):
    def makeAlgorithm(k1, k2, m1, m2):
        slope = (k1 - k2) / (m2 - m1)
        intercept = k2 - m1 * slope
        def getK(wElo, lElo, wMatch, lMatch):
            return (max(min(intercept + slope * wMatch, k2), k1), max(min(intercept + slope * lMatch, k2), k1))
        return Algorithm(getK, "k1: " + str(k1) + ", k2: " + str(k2) + ", m1: " + str(m1) + ", m2: " + str(m2))
    tempList = []
    for k1 in k1s:
        for k2 in k2s:
            if k2 <= k1:
                continue
            for m1 in m1s:
                for m2 in m2s:
                    if m2 <= m1:
                        continue
                    tempList.append(makeAlgorithm(k1, k2, m1, m2))
    return tempList

def nonlinearMatchAlgorithmMaker(constants, offsets, shapes):
    def makeAlgorithm(c, o, s):
        def getK(wElo, lElo, wMatch, lMatch):
            return (c / (wMatch + o) ** s, c / (lMatch + o) ** s)
        return Algorithm(getK, "c: " + str(c) + ", o: " + str(o) + ", s: " + str(s))
    tempList = []
    for c in constants:
        for o in offsets:
            for s in shapes:
                tempList.append(makeAlgorithm(c, o, s))
    return tempList

def nonlinearEloAlgorithmMaker(constants, offsets, shapes):
    def makeAlgorithm(c, o, s):
        def getK(wElo, lElo, wMatch, lMatch):
            return (c if wElo <= o else c / (wElo - o) ** s, c if lElo <= o else c / (lElo - o) ** s)
        return Algorithm(getK, "c: " + str(c) + ", o: " + str(o) + ", s: " + str(s))
    tempList = []
    for c in constants:
        for o in offsets:
            for s in shapes:
                tempList.append(makeAlgorithm(c, o, s))
    return tempList

def getM(level, tRound, levelMs, roundMs):
    if level == "Grand Slam":
        levelM = levelMs[0]
    elif level == "ATP Finals":
        levelM = levelMs[1]
    elif level == "ATP Masters":
        levelM = levelMs[2]
    else:
        levelM = levelMs[3]
    if tRound == "Finals":
        roundM = roundMs[0]
    elif tRound == "Semi-Finals" or tRound == "Olympic Bronze":
        roundM = roundMs[1]
    elif tRound == "Quarter-Finals" or tRound == "Round Robin":
        roundM = roundMs[2]
    elif tRound == "Round of 16":
        roundM = roundMs[3]
    else:
        roundM = roundMs[4]
    return levelM * roundM
