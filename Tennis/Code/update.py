from datetime import datetime
from getMatches import exportMatches
from training import exportMatchesElo, runModel, exportHistory

if __name__ == "__main__":
    year = datetime.utcnow().year
    exportMatches(year)
    matchesElo, players, _, _ = runModel()
    exportHistory(matchesElo, players)
    exportMatchesElo(matchesElo)
