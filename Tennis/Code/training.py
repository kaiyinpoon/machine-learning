import csv
import datetime
import operator
import os
from math import *
from functools import reduce

def clean(name):
    return name.replace(" ", "").replace(".", "").replace("-", "").replace("'", "").replace('"', "").replace("(", "").replace(")", "").lower()

def getId(playerName, playerId):
    return clean(playerName)

class Algorithm:
    def __init__(self, algorithm, params):
        self.algorithm = algorithm
        self.params = params

def matchPlayed(score):
    if score == "(W/O)" or score == "W/O" or score == "both withdrew" or score == "(RET)" or score == "RET" or score == "(DEF)" or score == "(ABD)" or score == "(ABN)":
        return False
    return True

def getMatches(years):
    matches = []
    for year in years:
        paths = ["../Results/matches" + str(year) + ".csv", "../Results/MatchesMore/matchesMore" + str(year) + ".csv"]
        for path in paths:
            if os.path.exists(path):
                with open(path, newline='', encoding='utf-8') as f:
                    reader = csv.reader(f)
                    for row in reader:
                        matches.append(row)
    # Each tournament in each file is listed in reverse chronlogical order
    # Reverse stable sort by date, then reverse entire list
    return sorted(matches, key=lambda x: x[-4], reverse=True)[::-1]

def linearEloAlgorithmMaker(k1s, k2s, m1s, m2s):
    def makeAlgorithm(k1, k2, m1, m2):
        slope = (k1 - k2) / (m2 - m1)
        intercept = k2 - m1 * slope
        def getK(wElo, lElo, wMatch, lMatch):
            return (max(min(intercept + slope * wElo, k2), k1), max(min(intercept + slope * lElo, k2), k1))
        return Algorithm(getK, "k1: " + str(k1) + ", k2: " + str(k2) + ", m1: " + str(m1) + ", m2: " + str(m2))
    tempList = []
    for k1 in k1s:
        for k2 in k2s:
            if k2 <= k1:
                continue
            for m1 in m1s:
                for m2 in m2s:
                    if m2 <= m1:
                        continue
                    tempList.append(makeAlgorithm(k1, k2, m1, m2))
    return tempList

# Difference of +400 elo = 90% winning probability
def getELogistic(diff, scale = 182):
    return round(1 / (1 + e ** (-1 * diff / scale)), 4)

def step(wTuple, lTuple, getK, getE, importance):
    wElo, _, wMatch = wTuple
    lElo, _, lMatch = lTuple
    k = getK(wElo, lElo, wMatch, lMatch)
    wEv = getE(wElo - lElo)
    wElo += (k[0] * importance * (1 - wEv))
    lElo += (k[1] * importance * (0 - (1 - wEv)))
    return 1 - wEv, round(wElo, 4), round(lElo, 4)

class Match:
    def __init__(self, match, wElo, lElo, wEloPrev, lEloPrev):
        self.winner, self.loser, self.date, self.score, self.event = match[0], match[2], match[7], match[4], match[5]
        self.wId, self.lId = match[1], match[3]
        self.wElo, self.lElo = wElo, lElo
        self.wEloPrev, self.lEloPrev = wEloPrev, lEloPrev
    def __repr__(self):
        return ", ".join([self.winner, str(self.wEloPrev), str(self.wElo), self.loser, str(self.lEloPrev), str(self.lElo), self.date, self.score, self.event])

def isKnownPlayer(name):
    return "qualifierqualifier" not in clean(name) and "unknownunknown" not in clean(name)

def training(getK, getE, matches, getM = lambda a, b, c, d: 1, levelMs = [1, 1, 1, 1], roundMs = [1, 1, 1, 1, 1]):
    players = dict()
    sumSqError, count, right = 0, 0, 0
    matchesElo = []
    for match in matches:
        if not matchPlayed(match[4]):
            continue
        wId, lId = getId(match[0], match[1]), getId(match[2], match[3])
        if wId not in players:
            players[wId] = (1500, 1500, 1)
        if lId not in players:
            players[lId] = (1500, 1500, 1)
        importance = getM(match[6], match[-1], levelMs, roundMs)
        error, wElo, lElo = step(players[wId], players[lId], getK, getE, importance)
        matchesElo.append(Match(match, wElo, lElo, players[wId][0], players[lId][0]))
        y = int(match[7][:4])
        if y >= 2010:
            sumSqError += (error ** 2)
            if error == 0.5:
                right += 0.5
            elif error < 0.5:
                right += 1
            count += 1
        if isKnownPlayer(match[0]):
            players[wId] = (wElo, max(wElo, players[wId][1]), players[wId][2] + 1)
        if isKnownPlayer(match[2]):
            players[lId] = (lElo, players[lId][1], players[lId][2] + 1)
    rmse = (sumSqError / count) ** 0.5
    return matchesElo, players, rmse, right / count

def runModel(k1s=[20], k2s=[215], m1s=[2200], m2s=[2500]):
    years = range(1877, datetime.datetime.utcnow().year + 1)
    matches = getMatches(years)
    for k in linearEloAlgorithmMaker(k1s, k2s, m1s, m2s):
        matchesElo, players, rmse, precison = training(k.algorithm, getELogistic, matches)
        print(k.params, rmse, precison)
    return matchesElo, players, rmse, precison

def exportHistory(matchesElo, players, write=True):
    def rank(players):
        return sorted([(k, v[1]) for k, v in players.items()], key=lambda x: x[1], reverse=True)
    """ Compute top 20 players by peak elo """
    top20 = list(map(lambda x: x[0], rank(players)[:20]))
    top100 = list(map(lambda x: x[0], rank(players)[:100]))
    top100Matches = list(filter(lambda x: getId(x.winner, x.wId) in top100 or getId(x.loser, x.lId) in top100, matchesElo))
    top100Names = dict()
    while len(top100Names) < 100:
        for match in matchesElo:
            wId, lId = getId(match.winner, match.wId), getId(match.loser, match.lId)
            if wId in top100:
                top100Names[wId] = match.winner
            if lId in top100:
                top100Names[lId] = match.loser
    top20Dict = dict()
    for p in top20:
        top20Dict[top100Names[p]] = len(top20Dict)
    names = top20Dict.keys()
    eloByDate = [[top100Matches[0].date, [0 for _ in range(20)]]]
    for match in top100Matches:
        if match.winner or match.loser in names:
            if match.date != eloByDate[-1][0]:
                eloByDate.append([match.date, eloByDate[-1][1][:]])
            if match.winner in names:
                eloByDate[-1][1][top20Dict[match.winner]] = match.wElo
            if match.loser in names:
                eloByDate[-1][1][top20Dict[match.loser]] = match.lElo
    for i in range(20):
        index, date, lastElo = -1, "", eloByDate[-1][1][i]
        for j in range(len(eloByDate)):
            row = eloByDate[j]
            if row[1][i] == lastElo:
                index, date = i, datetime.datetime.strptime(row[0], "%Y.%m.%d")
                break
        for j in range(index, len(eloByDate)):
            row = eloByDate[j]
            rowDate = datetime.datetime.strptime(row[0], "%Y.%m.%d")
            if (rowDate - date).days > 365:
                eloByDate[j][1][i] = 0
    goats = map(lambda x: [x[0]] + x[1], eloByDate)
    if write:
        with open('top20PeakEloHistory.csv', 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["Date"] + list(map(lambda x: top100Names[x], top20)))
            writer.writerows(goats)

    """ Compute historical top 10 players (player not considered after 1 year of inactivity """
    currentDate = datetime.datetime.strptime(matchesElo[0].date, "%Y.%m.%d")
    actives = dict()
    rankingHistory = []
    for match in matchesElo:
        matchDate = datetime.datetime.strptime(match.date, "%Y.%m.%d")
        if matchDate != currentDate:
            actives = {k:v for k,v in actives.items() if v[2] >= currentDate - datetime.timedelta(365)}
            rankingHistory.append([currentDate.strftime("%Y.%m.%d"), list(reduce(lambda x, y: x + y, list(map(lambda x: (x[0], x[1]), sorted(actives.values(), key=lambda x: x[1], reverse=True)[:10]))))])
            currentDate = matchDate
        wId, lId = getId(match.winner, match.wId), getId(match.loser, match.lId)
        actives[wId] = (match.winner, match.wElo, matchDate)
        actives[lId] = (match.loser, match.lElo, matchDate)
    rankingHistory.append([currentDate.strftime("%Y.%m.%d"), list(reduce(lambda x, y: x + y, list(map(lambda x: (x[0], x[1]), sorted(actives.values(), key=lambda x: x[1], reverse=True)[:10]))))])
    currentDate = datetime.datetime.utcnow()
    rankings = list(map(lambda x: [x[0]] + x[1], rankingHistory))
    if write:
        with open('topTenHistory.csv', 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["Date", "1", "Elo", "2", "Elo", "3", "Elo", "4", "Elo", "5", "Elo", "6", "Elo", "7", "Elo", "8", "Elo", "9", "Elo", "10", "Elo"])
            writer.writerows(rankings)

    def getPeakRank(name):
        rank, date = "N/A", "N/A"
        for ranking in rankings:
            if name in ranking:
                if rank == "N/A" or (ranking.index(name) + 1) / 2 < rank:
                    rank = int((ranking.index(name) + 1) / 2)
                    date = ranking[0]
        return rank, date
    top100 = rank(players)[:100]
    for i in range(100):
        player, peakElo, pEDate, pEEvent, pEOpponent, pEScore = top100Names[top100[i][0]], top100[i][1], None, None, None, None
        peakRank, pRDate = getPeakRank(player)
        for match in top100Matches:
            if getId(match.winner, match.wId) == top100[i][0] and match.wElo == top100[i][1]:
                pEDate, pEEvent, pEOpponent, pEScore = match.date, match.event, match.loser, match.score
                top100[i] = (player, peakElo, pEDate, pEEvent, pEOpponent, pEScore, peakRank, pRDate)
                break
    if write:
        with open('top100PeakElo.csv', 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["Player", "Peak Elo", "Date", "Event", "Opponent", "Score", "Peak Rank", "Date"])
            writer.writerows(top100)

    """ Compute historical top player """
    topHistory = [[rankingHistory[0][0], rankingHistory[0][1][0], 0]]
    for row in rankingHistory:
        topPlayer = row[1][0]
        if topPlayer == topHistory[-1][1]:
            continue
        rowDate = datetime.datetime.strptime(row[0], "%Y.%m.%d")
        lastDate = datetime.datetime.strptime(topHistory[-1][0], "%Y.%m.%d")
        topHistory[-1][2] = (rowDate - lastDate).days
        topHistory.append([row[0], row[1][0], 0])
    lastDate = datetime.datetime.strptime(topHistory[-1][0], "%Y.%m.%d")
    currentDate = datetime.datetime.utcnow()
    topHistory[-1][2] = (currentDate - lastDate).days
    if write:
        with open('numberOneHistory.csv', 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["Date", "Player", "Reign"])
            writer.writerows(topHistory)

    """ Compute most days as top player """
    daysAsTop = dict()
    daysAsTopOpenEra = dict()
    for row in topHistory:
        if row[1] not in daysAsTop:
            daysAsTop[row[1]] = 0
        daysAsTop[row[1]] += row[2]
        if int(row[0][:4]) >= 1968:
            if row[1] not in daysAsTopOpenEra:
                daysAsTopOpenEra[row[1]] = 0
            daysAsTopOpenEra[row[1]] += row[2]
    daysAsTop = sorted(daysAsTop.items(), key=operator.itemgetter(1), reverse=True)
    daysAsTopOpenEra = sorted(daysAsTopOpenEra.items(), key=operator.itemgetter(1), reverse=True)
    if write:
        with open('daysAsNumberOne.csv', 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["Player", "Days"])
            writer.writerows(daysAsTop)
        with open('daysAsNumberOneOpenEra.csv', 'w', newline='') as f:
            writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["Player", "Days"])
            writer.writerows(daysAsTopOpenEra)

def exportMatchesElo(matchesElo, years=range(1877, datetime.datetime.utcnow().year + 1), write=True):
    matchesByYear = dict()
    for year in years:
        matchesByYear[year] = []
    for match in matchesElo:
        y = int(match.date[:4])
        if y in years:
            matchesByYear[y].append([match.date, match.winner, match.wId, match.wEloPrev, match.wElo, match.loser, match.lId, match.lEloPrev, match.lElo, match.event, match.score])
    if write:
        for year in years:
            with open("../ResultsElo/matches" + str(year) + ".csv", 'w', newline='') as f:
                writer = csv.writer(f, quoting=csv.QUOTE_MINIMAL)
                writer.writerows(matchesByYear[year])

if __name__ == "__main__":
    matchesElo, players, rmse, precison = runModel()
    #exportMatchesElo(matchesElo, write=True)
    #exportHistory(matchesElo, players, write=True)
