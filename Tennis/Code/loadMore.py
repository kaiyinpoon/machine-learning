import csv
from getMatches import Match

def clean(name):
    return name.replace(" ", "").replace(".", "").replace("-", "").replace("'", "").replace('"', "").replace("(", "").replace(")", "").lower()

players = dict()
for year in range(1877,2020):
    with open("../Results/matches" + str(year) + ".csv", 'r') as csvFile:
        csvReader = csv.reader(csvFile, delimiter=',')
        for row in csvReader:
            players[clean(row[0])] = (row[0], row[1])
            players[clean(row[2])] = (row[2], row[3])

matches = []
with open("../Results/matchesMore.csv", 'r') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    for row in csvReader:
        matches.append(row)
matches = sorted(matches, key=lambda x: x[0][-10:])
last = ""
for match in matches:
    if match[-1]:
        last = match[-1]
    else:
        match[-1] = last
for match in matches:
    if clean(match[1]) in players:
        match.append(players[clean(match[1])][1])
    else:
        match.append("")
    if clean(match[2]) in players:
        match.append(players[clean(match[2])][1])
    else:
        match.append("")

def s(substr):
    for k, v in players.items():
        if substr in k.lower():
            print(v)

matchesDict = dict()
def getT(matchM):
    return (matchM[0][-10:-6], matchM[1], matchM[2], matchM[3])
for match in matches:
    t = getT(match)
    if t not in matchesDict:
        matchesDict[t] = [match[0]]
    else:
        matchesDict[t].append(match[0])

def getT2(year, match):
    return (year, match[0], match[2], match[4])
years = list(set(map(lambda x: x[0], matchesDict.keys())))
for year in years:
    matchesYear = dict()
    with open("../Results/matches" + str(year) + ".csv", 'r') as csvFile:
        csvReader = csv.reader(csvFile, delimiter=',')
        for row in csvReader:
            t = getT2(year, row)
            if t not in matchesYear:
                matchesYear[t] = [row[5] + '.' + row[7]]
            else:
                matchesYear[t].append(row[5] + '.' + row[7])
        if set(matchesYear.keys()).intersection(matchesDict.keys()):
            print(set(matchesYear.keys()).intersection(matchesDict.keys()))

tournaments = dict()
with open("../Results/tournamentsMore.csv", 'r') as csvFile:
    csvReader = csv.reader(csvFile, delimiter=',')
    for row in csvReader:
        tournaments[row[0] + '.' + row[2]] = row

matchesByYear = dict()
for match in matches:
    y = match[0][-10:-6]
    if y not in matchesByYear:
        matchesByYear[y] = []
    winner, loser, score, tRound = match[1], match[2], match[3], match[4]
    winnerId, loserId = match[-2], match[-1]
    t = tournaments[match[0]]
    tName, tLevel, tDate, tRoof, tSurface = t[0], t[1], t[2], t[3], t[4]
    matchesByYear[y].append([winner, winnerId, loser, loserId, score, tName, tLevel, tDate, tRoof, tSurface, tRound])

for year, matches in matchesByYear.items():
    with open("../Results/MatchesMore/matchesMore" + year + ".csv", 'w', newline='') as csvFile:
        csvWriter = csv.writer(csvFile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        csvWriter.writerows(matches)
