import time

def isSquare(number):
    if number < 0:
        return False
    return number == round(number ** 0.5) ** 2

x = [x ** 2 for x in range(0, 801)]
for a in x:
    print(a, time.strftime("%H:%M:%S"))
    for b in x:
        if b == a:
            continue
        for c in x:
            if c == a or c == b:
                continue
            for d in x:
                if b % 2 != d % 2 or d == a or d == b or d == c:
                    continue
                g = b + c - d
                if not isSquare(g) or g == a or g == b or g == c or g == d:
                    continue
                e = a + d - c
                if not isSquare(e) or e == a or e == b or e == c or e == d or e == g:
                    continue
                h = a + c - e
                if not isSquare(h) or h == a or h == b or h == c or h == d or h == g or h == e:
                    continue
                i = b + c - e
                if not isSquare(i) or i == a or i == b or i == c or i == d or i == g or i == e or i == h:
                    continue
                f = a + g - e
                if not isSquare(f) or f == a or f == b or f == c or f == d or f == g or f == e or f == h or f == i:
                    continue
                if g + h != c + f or c + f != a + e:
                    continue
                print(a, b, c, d, e, f, g, h, i)
