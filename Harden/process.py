import pandas as pd
import datetime

bday = datetime.date(1989, 8, 26)
def get_days(dateStr):
    return (datetime.datetime.strptime(dateStr[:10], "%Y-%m-%d").date() - bday).days

def get_season(dateStr):
    dateObj = datetime.datetime.strptime(dateStr[:10], "%Y-%m-%d").date()
    faDate = datetime.date(dateObj.year, 7, 1)
    if faDate > dateObj:
        return dateObj.year
    else:
        return dateObj.year + 1

codeToName = {
    "ATL": "Atlanta Hawks",
    "BOS": "Boston Celtics",
    "NJN": "New Jersey Nets",
    "BRK": "Brooklyn Nets",
    "CHA": "Charlotte Bobcats",
    "CHO": "Charlotte Hornets",
    "CHI": "Chicago Bulls",
    "CLE": "Cleveland Cavaliers",
    "DAL": "Dallas Mavericks",
    "DEN": "Denver Nuggets",
    "DET": "Detroit Pistons",
    "GSW": "Golden State Warriors",
    "HOU": "Houston Rockets",
    "IND": "Indiana Pacers",
    "LAC": "Los Angeles Clippers",
    "LAL": "Los Angeles Lakers",
    "MEM": "Memphis Grizzlies",
    "MIA": "Miami Heat",
    "MIL": "Milwaukee Bucks",
    "MIN": "Minnesota Timberwolves",
    "NOH": "New Orleans Hornets",
    "NOP": "New Orleans Pelicans",
    "NYK": "New York Knicks",
    "OKC": "Oklahoma City Thunder",
    "ORL": "Orlando Magic",
    "PHI": "Philadelphia 76ers",
    "PHO": "Phoenix Suns",
    "POR": "Portland Trail Blazers",
    "SAC": "Sacramento Kings",
    "SAS": "San Antonio Spurs",
    "TOR": "Toronto Raptors",
    "UTA": "Utah Jazz",
    "WAS": "Washington Wizards",
}

rtgByYr = {
    2010: {},
    2011: {},
    2012: {},
    2013: {},
    2014: {},
    2015: {},
    2016: {},
    2017: {},
    2018: {},
    2019: {}
}

scRtg = {
    "ATL": [4, 3.9, 3.7],
    "BOS": [4.7, 4.7, 4.2],
    "NJN": [4.6, 3.9, 3.7],
    "BRK": [4.1, 4, 3.9],
    "CHA": [3.6, 3.5, 3.5],
    "CHO": [3.6, 3.5, 3.5],
    "CHI": [4.6, 4.6, 4.5],
    "CLE": [3.8, 3.5, 3.5],
    "DAL": [4.6, 4, 4],
    "DEN": [5, 4.1, 3.8],
    "DET": [4.4, 4.1, 4],
    "GSW": [4.7, 4.3, 3.9],
    "HOU": [4.7, 4.4, 4],
    "IND": [4.1, 3.8, 3.6],
    "LAC": [4.7, 4.7, 4.4],
    "LAL": [4.7, 4.7, 4.4],
    "MEM": [4.1, 4, 3.8],
    "MIA": [5, 4.6, 4.6],
    "MIL": [4.6, 4.1, 3.8],
    "MIN": [4.1, 4.1, 3.7],
    "NOH": [4.8, 4.7, 4.5],
    "NOP": [4.8, 4.7, 4.5],
    "NYK": [5, 4.6, 4],
    "OKC": [4.4, 4.1, 4],
    "ORL": [5, 4.8, 3.8],
    "PHI": [4.6, 4.5, 3.8],
    "PHO": [4.4, 4.1, 4.1],
    "POR": [4.3, 4.1, 4.1],
    "SAC": [4.2, 4.1, 3.7],
    "SAS": [4.3, 4, 4],
    "TOR": [3.7, 3.5, 3.4],
    "UTA": [4.7, 4.5, 4],
    "WAS": [4.4, 4.1, 4.1]
}

ratings = ["0910.csv","1011.csv","1112.csv","1213.csv","1314.csv","1415.csv","1516.csv","1617.csv","1718.csv","1819.csv"]
for rating in ratings:
    rating_df = pd.read_csv(rating)
    for _, row in rating_df.iterrows():
        key = int("20" + rating[2:4])
        rtgByYr[key][row["Team"]] = (row["ORtg"], row["DRtg"], row["ORtg/A"], row["DRtg/A"])

df = pd.read_csv("harden.csv")
days = list(map(get_days, df["Date"]))
prev_days = [days[0] - 4] + days[:-1]
rest_days = list(map(lambda i: min(3, days[i] - prev_days[i] - 1), range(len(days))))
is_home = list(map(lambda x: 0 if x == "@" else 1, df["Unnamed: 4"]))
topOver45 = list(map(lambda x: 1 if scRtg[x][0] >= 4.5 else 0, df["Opp"]))
topRtg = list(map(lambda x: scRtg[x][0], df["Opp"]))
top3Over4 = list(map(lambda x: 1 if sum(scRtg[x]) >= 12 else 0, df["Opp"]))
top3Rtg = list(map(lambda x: round(sum(scRtg[x]), 1), df["Opp"]))

ortg = [0 for _ in range(len(df))]
drtg = [0 for _ in range(len(df))]
ortga = [0 for _ in range(len(df))]
drtga = [0 for _ in range(len(df))]
for index, row in df.iterrows():
    rtg = rtgByYr[get_season(row["Date"])][codeToName[row["Opp"]]]
    ortg[index] = rtg[0]
    drtg[index] = rtg[1]
    ortga[index] = rtg[2]
    drtga[index] = rtg[3]
    
df["Days"] = days
df["Rest"] = rest_days
df["IsHome"] = is_home
df["ORtg"] = ortg
df["DRtg"] = drtg
df["ORtgA"] = ortga
df["DRtgA"] = drtga
df["TopOver45"] = topOver45
df["TopRtg"] = topRtg
df["Top3Over4"] = top3Over4
df["Top3Rtg"] = top3Rtg

df_new = df[['MP', 'Days', 'Rest', 'IsHome', 'ORtg', 'DRtg', 'ORtgA', 'DRtgA', 'TopOver45', 'TopRtg', 'Top3Over4', 'Top3Rtg', 'GmSc']]
df_new.to_csv("harden_new.csv", index=False)
