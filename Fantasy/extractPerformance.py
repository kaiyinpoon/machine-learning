import csv, urllib.request, datetime, json
from os import listdir
from os.path import isfile, join

yesterday = (datetime.datetime.today() - datetime.timedelta(1)).strftime('%Y-%m-%d')
prefix = "https://rotogrinders.com/players/"
performance = dict()
times = ["1200", "1430", "1500", "1530", "1600", "1630", "1700", "1730", "1800", "1830"]
path = "CSV\\Projections\\"
projectionFiles = [f for f in listdir(path) if isfile(join(path, f)) and yesterday + "-projections" in f and "slate" not in f]

for time in times:
    projectionTimeFiles = list(filter(lambda x: time in x, projectionFiles))
    if len(projectionTimeFiles) > 0:
        with open(path + projectionTimeFiles[0], "r") as csvfile:
            players = list(csv.reader(csvfile))[1:]
        if len(performance) == 0:
            for player in players:
                url = prefix + player[1]
                content = urllib.request.urlopen(url).read().decode("utf-8")
                start = content.index("stats:") + 5
                end = content[start:].index("renderReact") + start - 25
                obj = json.loads('{"stats"' + content[start:end] + '}')
                for game in obj["stats"]["this-season"]:
                    if game["date"] == yesterday:
                        points = game["pts"] + game["3pm"] * 0.5 + game["reb"] * 1.25 + game["ast"] * 1.5 + game["stl"] * 2 + game["blk"] * 2 - game["to"] * 0.5 + game["dd"] * 1.5 + game["td"] * 3
                        performance[player[1]] = points
                        player[-1] = points
        else:
            for player in players:
                if player[1] in performance:
                    player[-1] = performance[player[1]]
        with open("CSV\\Performance\\" + yesterday + "-performance-" + time + ".csv", "w", newline='') as csvfile:
            writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
            writer.writerow(["nameCol","idCol","salaryCol","positionCol","dvpCol","ouCol","mpgCol","paceCol","usgCol","pminCol","ceilCol","floorCol","pointsCol","predictCol","actualCol"])
            writer.writerows(players)
        with open("CSV\\Performance\\all-performance-" + time + ".csv", "a", newline='') as csvfile:
            writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
            writer.writerows(players)

