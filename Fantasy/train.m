clear ; close all; clc
[nameCol,idCol,salaryCol,positionCol,dvpCol,ouCol,mpgCol,paceCol,usgCol,pminCol,ceilCol,floorCol,pointsCol,predictCol,y] = textread("CSV/Performance/all-performance-1800.csv", "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", "delimiter", ",", "headerlines", 1);
salaryCol = str2double(strrep(salaryCol,'"',''));
missingIndices = find(isnan(salaryCol) == 1);
dvpCol = str2double(strrep(dvpCol,'"',''));
missingIndices = union(missingIndices, find(isnan(dvpCol) == 1));
ouCol = str2double(strrep(ouCol,'"',''));
missingIndices = union(missingIndices, find(isnan(ouCol) == 1));
mpgCol = str2double(strrep(mpgCol,'"',''));
missingIndices = union(missingIndices, find(isnan(mpgCol) == 1));
paceCol = str2double(strrep(paceCol,'"',''));
missingIndices = union(missingIndices, find(isnan(paceCol) == 1));
usgCol = str2double(strrep(usgCol,'"',''));
missingIndices = union(missingIndices, find(isnan(usgCol) == 1));
pminCol = str2double(strrep(pminCol,'"',''));
missingIndices = union(missingIndices, find(isnan(pminCol) == 1));
ceilCol = str2double(strrep(ceilCol,'"',''));
missingIndices = union(missingIndices, find(isnan(ceilCol) == 1));
floorCol = str2double(strrep(floorCol,'"',''));
missingIndices = union(missingIndices, find(isnan(floorCol) == 1));
pointsCol = str2double(strrep(pointsCol,'"',''));
missingIndices = union(missingIndices, find(isnan(pointsCol) == 1));
y = str2double(strrep(y,'"',''));
y(missingIndices) = [];
X = [salaryCol dvpCol ouCol mpgCol paceCol usgCol pminCol ceilCol floorCol pointsCol];
X(missingIndices, :) = [];

#{
results = zeros(10, 10);
lambdas = 1:10;
for i = 1:10
  lambda = lambdas(i);
  for j = 1:10
    nRows = length(y);
    nSamples = round(nRows * .2);
    shuffle = randperm(nRows);
    validationX = X(shuffle(1:nSamples),:);
    validationY = y(shuffle(1:nSamples));
    trainingX = X(shuffle((nSamples + 1):nRows),:);
    trainingY = y(shuffle((nSamples + 1):nRows));

    % Need means and SDs to standardize new input after model is trained
    trainingMeans = mean(trainingX);
    trainingSds = std(trainingX);
    trainingX = bsxfun(@rdivide, bsxfun(@minus, trainingX, trainingMeans), trainingSds);
    nTrainingRows = size(trainingX, 1);

    validationMeans = mean(validationX);
    validationSds = std(validationX);
    validationX = bsxfun(@rdivide, bsxfun(@minus, validationX, validationMeans), validationSds);
    nValidationRows = size(validationX, 1);

    input_layer_size  = size(trainingX, 2);   % 10 features
    hidden_layer_size = 20;           % 20 hidden units
    num_labels = 1;                   % this is a regression problem

    initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
    initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);
    % Unroll parameters
    initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

    % Check gradients by running checkNNGradients
    % checkNNGradientsRegression;
    % lambda = 3;
    % checkNNGradientsRegression(lambda);

    % After you have completed the assignment, change the MaxIter to a larger
    % value to see how more training helps.
    options = optimset('MaxIter', 250);

    % Create "short hand" for the cost function to be minimized
    costFunction = @(p) nnCostFunctionRegression(p, ...
                                       input_layer_size, ...
                                       hidden_layer_size, ...
                                       num_labels, trainingX, trainingY, lambda);
    [nn_params, cost] = fmincg(costFunction, initial_nn_params, options);
    Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                     hidden_layer_size, (input_layer_size + 1));
    Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                     num_labels, (hidden_layer_size + 1));

    pred = predictRegression(Theta1, Theta2, trainingX);
    fprintf('\nTraining Set MSE: %f\n', sum((pred - trainingY) .^ 2) / nTrainingRows);

    pred = predictRegression(Theta1, Theta2, validationX);
    fprintf('\nValidation Set MSE: %f\n', sum((pred - validationY) .^ 2) / nValidationRows);
    results(j, i) = sum((pred - validationY) .^ 2) / nValidationRows;

    newPointsCol = pointsCol(:);
    newPointsCol(missingIndices) = [];
    newPointsCol = newPointsCol(shuffle(1:nSamples));
    fprintf('\nProjection MSE: %f\n', sum((newPointsCol - validationY) .^ 2) / nValidationRows);
  endfor
endfor

resultMeans = mean(results);
#}

#{
% trained lambdas
% 1200  2
% 1430  8
% 1500  5
% 1530  9
% 1600  4
% 1630  8
% 1700  5
% 1730  7
% 1800  3
% 1830  6
#}


lambda = 3;

input_layer_size  = size(X, 2);   % 10 features
hidden_layer_size = 20;           % 20 hidden units
num_labels = 1;                   % this is a regression problem

initial_Theta1 = randInitializeWeights(input_layer_size, hidden_layer_size);
initial_Theta2 = randInitializeWeights(hidden_layer_size, num_labels);
% Unroll parameters
initial_nn_params = [initial_Theta1(:) ; initial_Theta2(:)];

options = optimset('MaxIter', 500);

% Create "short hand" for the cost function to be minimized
costFunction = @(p) nnCostFunctionRegression(p, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, X, y, lambda);
[nn_params, cost] = fmincg(costFunction, initial_nn_params, options);
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));
Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));
