clear ; close all; clc
load train-1830.mat
[nameCol,idCol,salaryCol,positionCol,dvpCol,ouCol,mpgCol,paceCol,usgCol,pminCol,ceilCol,floorCol,pointsCol,predictCol,y] = textread("C:/Users/kaiyinpoon/Dropbox/Public/Academic/Fantasy/CSV/Projections/2018-12-21-projections-slate-23442-1830.csv", "%s %s %s %s %s %s %s %s %s %s %s %s %s %s %s", "delimiter", ",", "headerlines", 1);
salaryCol = str2double(strrep(salaryCol,'"',''));
missingIndices = find(isnan(salaryCol) == 1);
dvpCol = str2double(strrep(dvpCol,'"',''));
missingIndices = union(missingIndices, find(isnan(dvpCol) == 1));
ouCol = str2double(strrep(ouCol,'"',''));
missingIndices = union(missingIndices, find(isnan(ouCol) == 1));
mpgCol = str2double(strrep(mpgCol,'"',''));
missingIndices = union(missingIndices, find(isnan(mpgCol) == 1));
paceCol = str2double(strrep(paceCol,'"',''));
missingIndices = union(missingIndices, find(isnan(paceCol) == 1));
usgCol = str2double(strrep(usgCol,'"',''));
missingIndices = union(missingIndices, find(isnan(usgCol) == 1));
pminCol = str2double(strrep(pminCol,'"',''));
missingIndices = union(missingIndices, find(isnan(pminCol) == 1));
ceilCol = str2double(strrep(ceilCol,'"',''));
missingIndices = union(missingIndices, find(isnan(ceilCol) == 1));
floorCol = str2double(strrep(floorCol,'"',''));
missingIndices = union(missingIndices, find(isnan(floorCol) == 1));
pointsCol = str2double(strrep(pointsCol,'"',''));
missingIndices = union(missingIndices, find(isnan(pointsCol) == 1));
y = str2double(strrep(y,'"',''));
y(missingIndices) = [];
X = [salaryCol dvpCol ouCol mpgCol paceCol usgCol pminCol ceilCol floorCol pointsCol];
X(missingIndices, :) = [];
pred = predictRegression(Theta1, Theta2, X);