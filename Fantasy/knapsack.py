import csv, urllib.request, datetime, json, datetime
from os import listdir
from os.path import isfile, join

def getId(player):
    return player[0]
def getSalary(player):
    return player[1]
def getPoints(player):
    return player[3]
getLineupSalary = lambda x: getSalary(x)
getLineupPoints = lambda x: getPoints(x)

""" Returns True if player A is better than player B in all areas """
def isBetter(A, B):
    return getPoints(A) >= getPoints(B) and getSalary(A) <= getSalary(B) and B[2] in A[2]

def hasRepeat(k1, k2):
    sorted1, sorted2 = k1.split(","), k2.split(",")
    index1, index2 = 0, 0
    while index1 < len(sorted1) and index2 < len(sorted2):
        item1, item2 = sorted1[index1], sorted2[index2]
        if item1 == item2:
            return True
        if item1 < item2:
            index1 += 1
        else:
            index2 += 1
    return False

def preProcess(projectionFile):
    with open(path + projectionFile, "r") as csvfile:
        players = list(csv.reader(csvfile))[1:]

    # use prediction column
    # utils = list(map(lambda x: (x[1], x[2], x[3], x[-2]), players))
    utils = list(map(lambda x: (x[1], int(x[2]), x[3], float(x[-3]), round(float(x[-3]) / int(x[2]) * 1000, 4)), players))
    utils = sorted(utils, reverse = True, key = lambda x: x[4])

    inferior = set()
    for i in range(len(utils)):
        if utils[i][-1] < 4:
            inferior.add(utils[i])
    for each in inferior:
        utils.remove(each)
    inferior = set()
    if len(utils) > 40:
        for i in range(len(utils)):
            for j in range(len(utils)):
                if i != j:
                    if isBetter(utils[i], utils[j]) and utils[j][-1] < 4.5:
                        inferior.add(utils[j])
    for each in inferior:
        utils.remove(each)
    return utils[:40]

def process(projectionFile):
    utils = preProcess(projectionFile)

    pgs = set(filter(lambda x: "PG" in x[2], utils))
    sgs = set(filter(lambda x: "SG" in x[2], utils))
    gs = pgs.union(sgs)
    sfs = set(filter(lambda x: "SF" in x[2], utils))
    pfs = set(filter(lambda x: "PF" in x[2], utils))
    fs = sfs.union(pfs)
    cs = set(filter(lambda x: "C" in x[2], utils))

    salaries = sorted(map(getSalary, utils))
    points = sorted(map(getPoints, utils), reverse = True)
    fourCheapest = sum(salaries[:4])
    fourBest = sum(points[:4])

    pgsggutil = dict()
    for pg in pgs:
        for sg in sgs:
            if pg[0] == sg[0]:
                continue
            for g in gs:
                if g[0] == pg[0] or g[0] == sg[0]:
                    continue
                for util in utils:
                    if util[0] != pg[0] and util[0] != sg[0] and util[0] != g[0]:
                        lineup = (pg, sg, g, util)
                        lineupSalary = sum(map(getLineupSalary, lineup))
                        lineupPoints = sum(map(getLineupPoints, lineup))
                        if lineupSalary + fourCheapest <= salaryCap and lineupPoints + fourBest >= goodScore:
                            ids = sorted(map(getId, lineup))
                            pgsggutil[",".join(ids)] = (lineupSalary, lineupPoints, pg[0], sg[0], g[0], util[0])
    sfpffc = dict()
    for sf in sfs:
        for pf in pfs:
            if sf[0] == pf[0]:
                continue
            for f in fs:
                if f[0] == sf[0] or f[0] == pf[0]:
                    continue
                for c in cs:
                    if c[0] != sf[0] and c[0] != pf[0] and c[0] != f[0]:
                        lineup = (sf, pf, f, c)
                        lineupSalary = sum(map(getLineupSalary, lineup))
                        lineupPoints = sum(map(getLineupPoints, lineup))
                        if lineupSalary + fourCheapest <= salaryCap and lineupPoints + fourBest >= goodScore:
                            ids = sorted(map(getId, lineup))
                            sfpffc[",".join(ids)] = (lineupSalary, lineupPoints, sf[0], pf[0], f[0], c[0])

    estimateRuntimeSec = len(pgsggutil) * len(sfpffc) / 445700

    goodLineups = dict()
    for k2, v2 in sfpffc.items():
        for k1, v1 in pgsggutil.items():
            if not hasRepeat(k1, k2) and v1[0] + v2[0] <= salaryCap and v1[1] + v2[1] >= goodScore:
                lineupId = ",".join(sorted(v1[2:] + v2[2:]))
                if lineupId not in goodLineups:
                    goodLineups[lineupId] = (v1[2], v1[3], v2[2], v2[3], v2[5], v1[4], v2[4], v1[5], v1[0] + v2[0], round(v1[1] + v2[1], 2))
    goodLineups = sorted(goodLineups.values(), reverse = True, key = lambda x: x[-1])

    topFive = []
    add = True
    for lineup in goodLineups:
        if len(topFive) == 5:
            break
        for each in topFive:
            if len(set(lineup[:8]).intersection(each[:8])) >= 7:
                add = False
        if add:
            topFive.append(lineup)
            
    with open("CSV\\Lineups\\" + projectionFile.replace("projections", "lineups"), "w", newline='') as csvfile:
        writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
        writer.writerows(topFive)
        writer.writerow([])
        writer.writerows(goodLineups[:100])
    return goodLineups

today = datetime.datetime.today().strftime('%Y-%m-%d')

now = datetime.datetime.now()
time = now.replace(microsecond=0, second=0, minute=(30 if now.minute >= 30 else 0)).strftime("%H%M")
salaryCap = 50000

path = "CSV\\Projections\\"
projectionFiles = [f for f in listdir(path) if isfile(join(path, f)) and today + "-projections-slate" in f]
for projectionFile in projectionFiles:
    if time in projectionFile:
        goodScore = 265
        goodLineups = process(projectionFile)
        if len(goodLineups) < 5:
            goodScore = 255
            process(projectionFile)
