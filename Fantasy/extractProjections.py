import csv, urllib.request, datetime, json, re

slates = set()

class Player:
    def __init__(self, playerId, playerName, salary, position, dvp, ou, mpg, pace, usg, pmin, ceil, floor, points):
        self.playerId, self.playerName, self.salary, self.position = playerId, playerName, salary, position
        self.dvp, self.ou, self.mpg, self.pace, self.usg, self.pmin, self.ceil = dvp, ou, mpg, pace, usg, pmin, ceil
        self.floor, self.points, self.slates = floor, points, []

def parsePlayer(json):
    playerName = json["player_name"]
    playerId = playerName.replace(" ", "-") + "-" + str(json["player_id"])
    position, dvp = json["position"], json["dvp"]
    data = json["import_data"]
    salary = 0
    for datum in data:
        if datum["type"] == "classic":
            salary = int(float(datum["salary"]))
    if salary == 0:
        salary = json["salary"]
    pmin, ceil, floor, points = json["pmin"], round(json["ceil"], 2), round(json["floor"], 2), json["points"]
    last2weeks = json["2weeks"] if "2weeks" in json else dict()
    mpg = last2weeks["mpg"] if "mpg" in last2weeks else 0
    pace = last2weeks["pace"] if "pace" in last2weeks else 0
    usg = last2weeks["usg"] if "usg" in last2weeks else 0
    ou = json["o/u"] if "o/u" in json else 0
    player = Player(playerId, playerName, salary, position, dvp, ou, mpg, pace, usg, pmin, ceil, floor, points)
    player.slates = list(filter(None, map(lambda x: x["slate_id"] if "classic" == x["type"] else 0, json["import_data"])))
    for s in player.slates:
        slates.add(s)
    return player

today = datetime.datetime.today().strftime('%Y-%m-%d')

now = datetime.datetime.now()
time = now.replace(microsecond=0, second=0, minute=(30 if now.minute >= 30 else 0)).strftime("%H%M")
url = "https://rotogrinders.com/projected-stats/nba-player?site=draftkings"
content = urllib.request.urlopen(url).read().decode("utf-8")

start = content.index("data = [{") + 8
end = content[start:].index("}];") + start + 1
string = content[start:end]
occurances = [(m.start() + 9) for m in re.finditer('4in5', string)]
starts = [0] + occurances[:-1]
ends = [(o - 1) for o in occurances]
endpoints = list(map(lambda i: (starts[i], ends[i]), range(len(starts))))
json = list(map(lambda endpoint: json.loads(string[endpoint[0]:endpoint[1]]), endpoints))
players = list(map(parsePlayer, json))

def getRow(player):
    return [player.playerName, player.playerId, player.salary, player.position, player.dvp, player.ou, player.mpg, player.pace, player.usg, player.pmin, player.ceil, player.floor, player.points, 0, 0]

with open("CSV\\Projections\\" + today + "-projections-" + time + ".csv", "w", newline='') as csvfile:
    writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
    writer.writerow(["nameCol","idCol","salaryCol","positionCol","dvpCol","ouCol","mpgCol","paceCol","usgCol","pminCol","ceilCol","floorCol","pointsCol","predictCol","actualCol"])
    writer.writerows(map(getRow, players))

for slate in slates:
    slatePlayers = list(filter(lambda player: slate in player.slates, players))
    with open("CSV\\Projections\\" + today + "-projections-slate-" + str(slate) + "-" + time + ".csv", "w", newline='') as csvfile:
        writer = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
        writer.writerow(["nameCol","idCol","salaryCol","positionCol","dvpCol","ouCol","mpgCol","paceCol","usgCol","pminCol","ceilCol","floorCol","pointsCol","predictCol","actualCol"])
        writer.writerows(map(getRow, slatePlayers))
