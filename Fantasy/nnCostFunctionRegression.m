function [J grad] = nnCostFunctionRegression(nn_params, ...
                                   input_layer_size, ...
                                   hidden_layer_size, ...
                                   num_labels, ...
                                   X, y, lambda)
%NNCOSTFUNCTION Implements the neural network cost function for a two layer
%neural network which performs regression
%   [J grad] = NNCOSTFUNCTONREGRESSION(nn_params, hidden_layer_size, num_labels, ...
%   X, y, lambda) computes the cost and gradient of the neural network. The
%   parameters for the neural network are "unrolled" into the vector
%   nn_params and need to be converted back into the weight matrices. 
% 
%   The returned parameter grad should be a "unrolled" vector of the
%   partial derivatives of the neural network.
%

% Reshape nn_params back into the parameters Theta1 and Theta2, the weight matrices
% for our 2 layer neural network
Theta1 = reshape(nn_params(1:hidden_layer_size * (input_layer_size + 1)), ...
                 hidden_layer_size, (input_layer_size + 1));

Theta2 = reshape(nn_params((1 + (hidden_layer_size * (input_layer_size + 1))):end), ...
                 num_labels, (hidden_layer_size + 1));

% Setup some useful variables
m = size(X, 1);

% You need to return the following variables correctly 
J = 0;
Theta1_grad = zeros(size(Theta1));
Theta2_grad = zeros(size(Theta2));

Delta1 = zeros(size(Theta1));
Delta2 = zeros(size(Theta2));
XX = [ones(m, 1) X];
A1 = XX * Theta1';
A1 = [ones(m, 1) A1];
A2 = A1 * Theta2';
J = sum((A2 - y) .^ 2) / 2 / m + lambda / 2 / m * (sum(sum(Theta1(:,2:end) .^ 2)) + sum(sum(Theta2(:,2:end) .^ 2)));

for i = 1:m
  a1 = XX(i,:)';
  a2 = A1(i,:)';
  a3 = A2(i,:)';
  delta3 = a3 - y(i);
  delta2 = (Theta2' * delta3);
  delta1 = (Theta1' * delta2(2:end));
  Delta2 = Delta2 + delta3 * a2';
  Delta1 = Delta1 + delta2(2:end) * a1';
endfor
Theta1_grad = Delta1 ./ m;
Theta2_grad = Delta2 ./ m;
Theta1_grad(:,2:end) = Theta1_grad(:,2:end) + Theta1(:,2:end) .* lambda ./ m;
Theta2_grad(:,2:end) = Theta2_grad(:,2:end) + Theta2(:,2:end) .* lambda ./ m;
% -------------------------------------------------------------

% =========================================================================

% Unroll gradients
grad = [Theta1_grad(:) ; Theta2_grad(:)];


end
